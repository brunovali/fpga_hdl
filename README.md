# FPGA_HDL

Inside the directory named *src* there are parts of four different projects.

### SATA
This is part of a SATA IP core, written in VHDL, is only included part of the link layer and the testbenches.
The project is done un pure vhdl, and was developed as a vendor independant IP core. For the first stages of the development it was used GHDL for the simulations and GTKWave for visualizations.

### gearbox
This is part of a project which involves the use of a Virtex-4 and a Zynq Ultrascale+.
The project contains part of the interface between two boards.
The gearbox is used to transmit 8 channels of 12bits width at 80 Mhz, generated in the Virtex 4, to an Ultrascale+ using the serdes.
This was done due to a constraint of the Ultrascale+, when working in DDR it only can transmit 8 bits width. It also counts on a pseudo bitslip generator, to readjust the bit framing.

The project was developed in vhdl, using Vivado and ISE, and as it makes usage of the serdes is product specific.

### pwm
This IP and the testbenches were developed in verilog for and OpalKelly development board (Spartan 3).
Is part of a control loop of High Voltage power source used for polarizing a medical device.
It was initially developed using iVerilog, but then for the final implementation with ISE.

### CDC
This IP is a Clock Domain Crossing data buffer developed for a Xilinx Zynq7000 family, in VHDL and is targeted for tackle fast data using specific FIFOs located near the IOs.
The design is used together with a constraint file to locate the IP in the same clock region as the IOs.

