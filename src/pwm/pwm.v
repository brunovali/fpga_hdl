//
// PWM
//
// Author(s):
// Bruno Valinoti
//
// Copyright (c) 2017 Authors and INTI
// Distributed under the BSD 3-Clause License
//
// Description:
//

module pwm #(parameter SYS_FREQ = 25_000_000, parameter RES = 24)
   (
   input  wire           clk_i,             //system clock
   input  wire           rst_i,             //system reset
   input  wire           oena_i,            //output enable
   input  wire [7:0]     dutcyc_i,          //duty cicle, must be an integer between 0 and 100
   input  wire [RES-1:0] freq_i,            //desired frequency
   input  wire           new_conf_i,        //new frequency/duty cycle change request
   output wire           pwm_o              //pwm output
   );

// -------------------------------------------
// Divider module instantiation
// -------------------------------------------
parameter     W = 32   ;
reg  [W-1:0]  divisor  ;
reg  [W-1:0]  dividend ;
reg           start    ;
wire          ready    ;
wire          div_done ;
wire [W-1:0]  quotient ;
wire [W-1:0]  remainder;

divider #(.W(W))
   DIV (
      .clk_i(clk_i),              //system clock
      .rst_i(rst_i),              //system reset
      .start_i(start),           //start division
      .divisor_i(divisor),      //divisor value
      .dividend_i(dividend),    //dividend value
      .ready_o(ready),          //divider ready to make de division
      .done_tick_o(div_done),   //division made signal
      .quo_o(quotient),         //division value
      .rmd_o(remainder)         //remainder value
   );


// -------------------------------------------
// PWM process & signals
// -------------------------------------------
reg  [RES-1:0] period;
integer        counter;
integer        N_pwm;
integer        N_d;
reg            pwm;
reg            conf_done;
reg   [1:0]    state_reg;

localparam [1:0]
    IDLE  = 2'd0,
    NpWM  = 2'd1,
    WAIT  = 2'd2,
    NdUTY = 2'd3;

// Period and end count calc     // Es medio pedorrona esta forma de calcularlo pero capaz que les alcanza pq
always @ (posedge clk_i) begin   //no me especificó nada acerca del error/precisión con la que necesitan el PWM
   if (rst_i) begin              //HACER para mañana
      state_reg <= IDLE;
      conf_done <= 1'b0;
   end else begin
      case (state_reg)
         IDLE : begin
            if (new_conf_i && ready) begin
               state_reg <= NpWM;
               dividend  <= SYS_FREQ;
               divisor   <= freq_i;
               start     <= 1'b1;
               conf_done <= 1'b0;
            end
         end
         NpWM : begin
            start <= 1'b0;
            if (div_done) begin
               N_pwm <= quotient;// >> 1;
               state_reg <= WAIT;
            end
         end
         WAIT : begin
            dividend  <= dutcyc_i * N_pwm;
            divisor   <= 100;
            state_reg <= NdUTY;
            start <= 1'b1;
         end
         NdUTY: begin
            start <= 1'b0;
            if (div_done) begin
               N_d  <= quotient;
               state_reg <= IDLE;
               conf_done <= 1'b1;
            end
         end
         default: state_reg <= IDLE;
      endcase
   end
end


// On/off output state
assign pwm_o = oena_i ? pwm : 1'b0;

always @ (posedge clk_i) begin
   if (rst_i || new_conf_i) begin
      counter <= 0;
      pwm   <= 1'b0;
   end else if (conf_done) begin
      pwm   <= 1'b0;
      if (counter < N_pwm-1) begin
         counter <= counter + 1;
         if (counter < N_d) begin
            pwm <= 1'b1;
         end
      end else begin
         counter <= 0;
      end
   end
end

endmodule //
