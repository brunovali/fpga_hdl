//
// divider
//
// Author(s): extracted from  FPGA prototyping using Verilog examples, Pong P. Chu
//
// Description: divider IP core
//

module divider #( parameter W = 8, parameter CBIT = 4)
   (
    input  wire          clk_i,
    input  wire          rst_i,
    input  wire          start_i,
    input  wire  [W-1:0] divisor_i,
    input  wire  [W-1:0] dividend_i,
    output reg           ready_o,
    output reg           done_tick_o,
    output wire  [W-1:0] quo_o,
    output wire  [W-1:0] rmd_o
   );

  localparam [1:0]
      IDLE = 2'd0,
      OP   = 2'd1,
      LAST = 2'd2,
      DONE = 2'd3;

  reg [1:0] state_reg, state_next;
  reg [W-1:0] rh_reg, rh_next, rl_reg, rl_next, rh_temp;
  reg [W-1:0] d_reg, d_next;
  reg [CBIT-1:0] n_reg, n_next;
  reg qbit;

  //state and data register FSM
  always @ (posedge clk_i or rst_i) begin
     if (rst_i) begin
        state_reg <= IDLE;
        rh_reg <= 0;
        rl_reg <= 0;
        d_reg  <= 0;
        n_reg  <= 0;
     end else begin
        state_reg <= state_next;
        rh_reg <= rh_next;
        rl_reg <= rl_next;
        d_reg  <= d_next;
        n_reg  <= n_next;
     end
  end

//Next state logic
  always @(*) begin
     state_next = state_reg;
     ready_o = 1'b0;
     done_tick_o = 1'b0;
     rh_next = rh_reg;
     rl_next = rl_reg;
     d_next = d_reg;
     n_next = n_reg;
     case (state_reg)
        IDLE: begin
           ready_o = 1'b1;
           if (start_i) begin
              rh_next = 0;
              rl_next = dividend_i;
              d_next = divisor_i;
              n_next = CBIT;
              state_next = OP;
           end
        end
        OP: begin
           rl_next = {rl_reg[W-2:0], qbit};
           rh_next = {rh_temp[W-2:0], rl_reg[W-1]};
           n_next = n_reg - 1;
           if (n_next == 1) begin
              state_next = LAST;
           end
        end
        LAST: begin
           rl_next = {rl_reg[W-2:0],qbit};
           rh_next = rh_temp;
           state_next = DONE;
        end
        DONE: begin
           done_tick_o = 1'b1;
           state_next = IDLE;
        end

        default: state_next = IDLE;
     endcase
  end

//compare and substract
  always @(*) begin
     if (rh_reg >= d_reg) begin
        rh_temp = rh_reg - d_reg;
        qbit = 1'b1;
     end else begin
        rh_temp = rh_reg;
        qbit = 1'b0;
     end
  end

  assign quo_o = rl_reg;
  assign rmd_o = rh_reg;

endmodule // divider
