//
// PWM Testbench
//
// Author(s):
//
//
// Copyright (c) 2017 Authors and INTI
// Distributed under the BSD 3-Clause License
//
// Description:
//  the test consist of 3 blocks:
//      1)when there is a new configuration it will calculate the expeted values.
//      2)when the pwm starts it will check the output value.
//      3)will waits the expected cycles and compare expected with simulation.
//

`timescale 10ns / 1ns
module pwm_tb ();

reg           clk;  //must simulate 25MHz system clock
reg           rst;
reg           oena;
wire          pwm;
reg           new_conf;
reg [RES-1:0] frequency;
reg [7:0]     duty_cycle;
parameter     RES = 24;
parameter     SYS_FREQUENCY = 50_000_000;
reg           new_data = 1'b0;

//clock generator
always @(clk)#5 clk <= ~clk;

// PWM Module instantiation
pwm  #(
       .SYS_FREQ(SYS_FREQUENCY),
       .RES(RES))
   PWM (
      .clk_i(clk),             //system clock
      .rst_i(rst),             //system reset
      .oena_i(oena),           //output enable
      .dutcyc_i(duty_cycle),   //duty cicle, must be an integer between 1 and 99
      .freq_i(frequency),      //desired frequency
      .new_conf_i(new_conf),   //new frequency/duty cycle change request
      .pwm_o(pwm)              //pwm output
   );

/* calculate the expected values of the PWM signal*/
integer TotalClkCounter = 0; //Total clk cycles of the PWM signal
integer TotalPwmOnCounter = 0; //Total clk cycles of the PWM ON signal
integer TotalPwmOffCounter = 0; //Total clk cycles of the PWM OFF signal

always @(posedge new_conf) begin
    TotalClkCounter = SYS_FREQUENCY/frequency;
    if (((TotalClkCounter*duty_cycle) % 100) != 0) begin
      $display("WARNING NOT POSIBLE TO CREATE EXPECTED DUTY CYCLE, ROUNDING DOWN");
    end
    TotalPwmOnCounter = TotalClkCounter*duty_cycle/100;
    TotalPwmOffCounter = TotalClkCounter - TotalPwmOnCounter;
    // to print expected values
    //$display("TotalClkCounter:%d TotalPwmOnCounter:%d TotalPwmOffCounter:%d",TotalClkCounter,TotalPwmOnCounter,TotalPwmOffCounter);
end

/* counter checker
   waits "N" cycles and compare expected values with simulation
*/
reg   [1:0]    state_reg;
localparam [1:0]
    IDLE  = 2'd0,
    COUNT = 2'd1;

integer       CheckCounter = 0;
integer       cycles = 1;
always @(posedge clk) begin
  if (PWM.conf_done == 1'b1) begin
      case (state_reg)
        IDLE:begin
          state_reg <=COUNT;
        end
        COUNT:begin
          //$display("CheckCounter:%d",CheckCounter);
          CheckCounter++;
          if (CheckCounter == TotalClkCounter) begin
            //$display("Complete cycle Number: %d",cycles);
            if (cycles != 1) begin//en el primer cyclo hay 1 pwmOFF de mas.
              if (TotalClkCounter != (PwmOnCounter +PwmOffCounter)/cycles) begin
                $display($time," PWM frequency ERROR Expected: %d, Simulation: %d",TotalClkCounter,(PwmOnCounter +PwmOffCounter)/cycles);
              end
              if (PwmOnCounter/cycles != TotalPwmOnCounter) begin
                $display($time," PWM ON ERROR Expected: %d, Simulation: %d",TotalPwmOnCounter,PwmOnCounter/cycles);
              end
              if ((PwmOffCounter)/cycles != TotalPwmOffCounter) begin
                $display($time," PWM OFF ERROR Expected: %d, Simulation: %d",TotalPwmOffCounter,PwmOffCounter/cycles);
              end
            end
            cycles++;
            CheckCounter = 0;
          end
        end
      endcase
  end else begin
    state_reg <= IDLE;
    cycles = 1;
    CheckCounter = 0;
  end
end


/* counter of PWM output signal On & Off */
integer       PwmOnCounter = 0;
integer       PwmOffCounter = 0;
always @(posedge clk) begin
  if (PWM.conf_done == 1'b1) begin
      if (pwm == 1'b1) begin
        PwmOnCounter++;
      end else begin
        PwmOffCounter++;
      end
  end else begin
    PwmOnCounter = 0;
    PwmOffCounter = 0;
  end
end



//Testbench section
initial
   begin
      $dumpfile("pwm_tb.vcd");
      $dumpvars(0,pwm_tb);
   fork
      clk    = 1'b0;
      rst    = 1'b1;
      oena   = 1'b0;
      new_conf = 1'b0;
      duty_cycle = 15;
      frequency = 10000;
   join
  //  --------------------
  // comienza testeo
  //  --------------------
  #35 rst = 1'b0;
  $display("comienza test");
  $display("Frecuencia del sistema: %d",SYS_FREQUENCY);
  $display("test 1: PWM frequency: %d, duty_cycle: %d",frequency,duty_cycle);
  @(posedge(clk));
  new_conf <= 1'b1;
  @(posedge(clk));
  new_conf <= 1'b0;
  oena <= 1'b1;
  @(posedge(clk));
  repeat (10000) @(posedge(clk));
  freq_rec <= 100000;
  dlt_rec  <= 25;
  new_data <= 1'b1;
  @(posedge(clk));
  new_data <= 1'b0;
  repeat(5*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  oena <= 1'b0;
  repeat(5*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  oena <= 1'b1;
  repeat(5*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  freq_rec <= 850000;
  dlt_rec  <= 33;
  new_data <= 1'b1;
  @(posedge(clk));
  new_data <= 1'b0;
  repeat(5*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  // oena <= 1'b0;
  // new_conf <= 1'b1;
  // duty_cycle <= 50;
  // frequency <= 100000;
  // @(posedge(clk));
  // $display("test2: PWM frequency: %d, duty_cycle: %d",frequency,duty_cycle);
  // new_conf <= 1'b0;
  // oena <= 1'b1;
  // repeat(10*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  // oena <= 1'b0;
  // //rst <= 1'b1;
  // new_conf <= 1'b1;
  // duty_cycle <= 25;
  // frequency <= 10500;
  // @(posedge(clk));
  // $display("test3: PWM frequency: %d, duty_cycle: %d",frequency,duty_cycle);
  // new_conf <= 1'b0;
  // oena <= 1'b1;
  // repeat(10*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  // new_conf <= 1'b1;
  // duty_cycle <= 50;
  // frequency <= 50000;
  // @(posedge(clk));
  // $display("test4: PWM frequency: %d, duty_cycle: %d",frequency,duty_cycle);
  // new_conf <= 1'b0;
  // oena <= 1'b1;
  // repeat(10*(SYS_FREQUENCY/frequency) + 10) @(posedge(clk));
  $finish;
end


//----------------------
// PWM reconfigure process
//----------------------
reg            aux_ena;
reg  [RES-1:0] freq_rec;
reg      [7:0] dlt_rec;
always @ (posedge clk) begin : PWM_RECONFIGURE_PROC
   if (rst) begin
      new_conf   <= 1'b1;
      aux_ena    <= 1'b0;
   end else begin
      new_conf   <= 1'b0;
      if (new_data == 1'b1) begin
         aux_ena    <= 1'b1;
         frequency  <= freq_rec;
         duty_cycle <= dlt_rec;
      end else begin
         aux_ena    <= 1'b0;
         frequency  <= frequency;
         duty_cycle <= duty_cycle;
      end
      if (aux_ena == 1'b1) begin
         aux_ena <= 1'b0;
         new_conf <= 1'b1;
      end
   end
end


endmodule
