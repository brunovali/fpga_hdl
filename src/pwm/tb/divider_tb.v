//
// Divider Testbench
//
// Author(s):
// Brunno Valinoti
//
// Copyright (c) 2017 Authors and INTI
// Distributed under the BSD 3-Clause License
//
// Description:
//

`timescale 10ns / 1ns
module divider_tb ();

reg           clk;  //must simulate 25MHz system clock
reg           rst;
reg           start;
reg  [W-1:0]  divisor;
reg  [W-1:0]  dividend;
wire          ready;
wire          div_done;
wire [W-1:0]  quotient;
wire [W-1:0]  remainder;
parameter     W = 8;

//clock generator
always @(clk)#40 clk <= ~clk;

// PWM Module instantiation
divider #(.W(W))
   DIV (
      .clk_i(clk),              //system clock
      .rst_i(rst),              //system reset
      .start_i(start),           //start division
      .divisor_i(divisor),      //divisor value
      .dividend_i(dividend),    //dividend value
      .ready_o(ready),          //divider ready to make de division
      .done_tick_o(div_done),   //division made signal
      .quo_o(quotient),         //division value
      .rmd_o(remainder)         //remainder value
   );




//Testbench section
initial
   begin
      $dumpfile("divider_tb.vcd");
      $dumpvars(0,divider_tb);
   fork
      clk     = 1'b0;
      rst     = 1'b1;
      start   = 1'b0;
      divisor = 1;
      dividend = 1;
   join
  //  --------------------
  // comienza testeo
  //  --------------------
  #350 rst = 1'b0;
  $display("comienza test");
  @(posedge(clk));

  divisor = 8'd22;
  dividend = 8'd137;
  if (ready == 1'b1) begin
    start = 1'b1;
  end else begin
    @(posedge(ready));
    start = 1'b1;
  end
  @(posedge(clk));
  start = 1'b0;
  @(posedge(div_done));

  if (quotient != dividend/divisor)
     $display("Error de division");
  else
     $display("La división entre %d y %d es: %d, el resto dió: %d", dividend, divisor, quotient, remainder);
  repeat(10) @(posedge(clk));
  $display("Fin del test.");
  $finish;
end
endmodule
