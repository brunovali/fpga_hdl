# Notas de implementación del PWM

## Parámetros y señales de entrada
- Frecuencia de reloj del sistema (**F_SYS**)
- Resolución del contador         (**RES**)
- Frecuencia del PWM              (**[RES-1:0] freq_i**)
- Ciclo de actividad              (**[7:0] dutcyc_i**)
- Clock del sistema               (**clk_i**)
- Reset del sistema               (**rst_i**)
- Entrada de habilitación         (**oena_i**)
- Pedido de nueva configuración   (**new_conf_i**)
- Salida del PWM                  (**pwm_o**)

**RES**   Es la cantidad de bits de la frecuencia deseada para el PWM.
**F_SYS** Es el valor de frecuencia del sistema.
**freq_i** Es la frecuencia del PWM.
**clk_i** Clock del sistema.
**rst_i** Reset de sistema.
**oena_i** Habilitación de señal de salida.
**pwm_o** Salida del PWM.

El core PWM utiliza como parámetros de entrada la frecuencia de PWM y el ciclo de actividad,
sabiendo la frecuencia de clock del sistema se hacen los siguientes cálculos. (Todas las cuentas
se hacen utilizando números enteros de períodos)

N_pwm: Cuenta para frecuencia del pwm, es un número entero
N_d: Cuenta para el estado activo del pwm, es un número entero.

N_pwm = F_sys / F_pwm   
N_d = (N_pwm * D)/100

En base a un contador a frecuencia de sistema se utilizan los valores de cuenta como definición del
ciclo de actividad y como período del pwm. El contador arranca con la salida del pwm en '1' y la
cuenta en cero, cuando es igual a **N_d** pone la salida en '0' y cuando alcanza al valor **N_pwm**
vuelve a poner la salida en '1' y resetea el valor de la cuenta a cero.

Para cargar un nuevo valor de frecuencia y ciclo de actividad se utiliza la entrada **new_conf_i**,
es activa en alto y se utiliza como un pulso, los valores **dutcyc_i y freq_i** deben cargarse
con valores válidos previo a la indicación de carga de nueva configuración.


#notas de funcionamiento:
En los casos EXTREMOS del duty cycle (0% y 100%):
	En 0% funciona bien.
	En 100% genera un PWM con la minima division posible en '0'(1 ciclo completo de la frecuencia 
	del sistema).

En los casos en que la resolucion no alcanza, el PWM redondea para abajo.
	Ej: 
		frecuencia del sistema = 100, frecuencia del PWM 10, Duty 10%
		El PWM se va a encontrar en '0' durante 9 ciclos de reloj, y en '1' en 1 ciclo de reloj.
		
		frecuencia del sistema = 100, frecuencia del PWM 10, Duty 15%
		El PWM se va a encontrar en '0' durante 9 ciclos de reloj, y en '1' en 1 ciclo de reloj,
		haciendo que el duty final sea del 10%.

Para toda frecuencia mayor que el 50% de la frecuencia del sistema, la salida permanece en '0'.