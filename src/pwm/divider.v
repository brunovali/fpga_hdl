//
// DIVIDER
//
// Author(s):
// Bruno Valinoti
//
// Copyright (c) 2017 Authors and INTI
// Distributed under the BSD 3-Clause License
//
// Description:
//
module divider #(parameter W = 8)
   (
    input  wire          clk_i,
    input  wire          rst_i,
    input  wire          start_i,
    input  wire  [W-1:0] divisor_i,
    input  wire  [W-1:0] dividend_i,
    output reg           ready_o,
    output reg           done_tick_o,
    output wire  [W-1:0] quo_o,
    output wire  [W-1:0] rmd_o
   );

   localparam [1:0]
       IDLE = 2'd0,
       OP   = 2'd1,
       LAST = 2'd2,
       DONE = 2'd3;

  reg [1:0] state_reg, state_next;
  reg [W-1:0] im_reg, im_next;
  reg [W-1:0] d_reg, d_next;
  reg [W-1:0] quot_reg, quot_next;



  //state and data register FSM  or rst_i
  always @ (posedge clk_i) begin
     if (rst_i) begin
        state_reg <= IDLE;
        im_reg <= 0;
        d_reg  <= 0;
        quot_reg <= 0;
     end else begin
        state_reg <= state_next;
        im_reg <= im_next;
        d_reg  <= d_next;
        quot_reg <= quot_next;
     end
  end


    always @(*) begin
       state_next = state_reg;
       ready_o = 1'b0;
       done_tick_o = 1'b0;
       im_next = im_reg;
       d_next = d_reg;
       quot_next = quot_reg;
       case (state_reg)
          IDLE: begin
             ready_o = 1'b1;
             if (start_i) begin
                im_next = dividend_i;
                d_next  = divisor_i;
                state_next = OP;
                quot_next = 0;
             end
          end
          OP: begin
             if (im_reg >= d_reg) begin
                im_next = im_reg - d_reg;
                quot_next = quot_reg + 1;
             end else begin
                state_next = LAST;
             end
          end
          LAST: begin
             state_next = DONE;
          end
          DONE: begin
             done_tick_o = 1'b1;
             state_next = IDLE;
          end

          default: state_next = IDLE;
       endcase
    end

    assign quo_o = quot_reg;
    assign rmd_o = im_reg;

  endmodule // divider
