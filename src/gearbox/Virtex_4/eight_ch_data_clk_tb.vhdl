-------------------------------------------------------------------------------
-- Title      : eight_ch_data_clk instantiation testing
-- Project    :
-------------------------------------------------------------------------------
-- File       : eight_ch_data_clk_tb
-- Author     : Bruno Valinoti -- bvalinot@ictp.it
-- Company    : ICTP
-- Created    : 15-03-2023
-- Last update: 
-- Platform   : VIrtex 4
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:
--              
--
-------------------------------------------------------------------------------
-- Copyright (c) 2023
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library UNISIM;
use UNISIM.vcomponents.all;
library work;
use work.i_adc_mainz_package.all;
use work.MSADC_RECORD_package.all;
use work.adc4ecal_package.all;


entity eight_ch_data_clk_tb is
end eight_ch_data_clk_tb;

architecture behavior of eight_ch_data_clk_tb is

 --Test signals
   signal clk        : std_logic := '0'; -- 80 MHz
   signal rst        : std_logic := '0';
	signal tx_rst     : std_logic;
   signal ena        : std_logic := '0';
   signal clk_p,clk_n: std_logic;
   
   signal ADC_data   : ADC_DATA12_type;
   signal data_0,data_1: std_logic_vector(11 downto 0);
	signal sync       : std_logic;
	signal valid      : std_logic;
 
   signal data_p: std_logic_vector(11 downto 0);
   signal data_n: std_logic_vector(11 downto 0);
   signal chA_select_in : std_logic_vector (3 downto 0):= "0000";
   signal chB_select_in : std_logic_vector (3 downto 0):= "0001";

-- additional signals
   signal tst_end    : boolean := false;
   signal time_counter : integer;
   constant PERIOD : time := 25 ns;    --80MHz
   constant FREQ_2 : time := PERIOD/2;


BEGIN
-- Support processes (clock and reset generation)
do_clock: process
begin
    clk <= '0';
    wait for FREQ_2;
    clk <= '1';
    wait for FREQ_2;
    if tst_end then
       wait;
    end if;
end process do_clock;

do_rst : process
begin
  rst <= '1';
  wait for 100 * PERIOD;
  rst <= '0';
  wait;
end process do_rst;


------------------------------------
---- Data genenrator and muxed data 
------------------------------------

  ADC_data <= (others=>(others =>'0'));
  ramp_data_generator_inst_A : entity work.ramp_data_generator
    port map (
      CLK_IN            => clk, -- This clock should come from outside and is the variable one
		RESET_IN          => rst,
      ADC_DATA_IN       => ADC_data,
		GEN_SELECT_IN     => '1',
      CHANNEL_SELECT_IN => chA_select_in,
      D_OUT             => data_0
      );
		
  ramp_data_generator_inst_B : entity work.ramp_data_generator
    port map (
      CLK_IN            => clk, -- This clock should come from outside and is the variable one
		RESET_IN          => rst,
      ADC_DATA_IN       => ADC_data,
		GEN_SELECT_IN     => '1',
      CHANNEL_SELECT_IN => chB_select_in,
      D_OUT             => data_1
      );

    data_n_clock_gen: entity work.eight_ch_data_clk
    port map(
       clk_i    => clk,  -- This clock is 80 MHz
       tx_rst_i => tx_rst,
       glbl_rst_i => not(rst),
		 sync_i   => sync,
		 data_0_i => data_0,
		 data_1_i => data_0,
		 data_2_i => data_1,
		 data_3_i => data_1,
		 data_4_i => data_1,
		 data_5_i => data_1,
		 data_6_i => data_0,
		 data_7_i => data_0,
		 --
		 valid_o => valid,
       --
       clk_p_o  => clk_p,
       clk_n_o  => clk_n,
       --
       data_p_o  => data_p,
       data_n_o  => data_n
    );

  do_test : process
  begin
     ena <= '1';
	  sync <= '0';
	  tx_rst <= '0';
     wait until rst = '0';
     wait until rising_edge(clk);
	  
	  tx_rst <= '1';
	  wait until rising_edge(clk);
	  wait until rising_edge(clk);
	  tx_rst <= '0';
	  wait until rising_edge(clk);
	  wait until valid = '1';
     -- for i in 0 to 255 loop
     --     assert cha_dat <= chb_dat
     --        report "BAD COUNT" 
     --        severity FAILURE;
     --     report "The value of 'cha_dat' is " & integer'image(to_integer(unsigned(cha_dat)));
     --     report "The value of 'chb_dat' is " & integer'image(to_integer(unsigned(chb_dat)));
     --     wait until rising_edge(valid);
     -- end loop;
     wait for 100 us;
	  sync <= '1';
	  wait for 1 ns;
	  wait until rising_edge(clk);
	  sync <= '0';
	  wait for 1 ns;
	  wait until rising_edge(clk);
     wait for 10 us;
     wait for 1 ns;
	  
	  tx_rst <= '1';
	  wait until rising_edge(clk);
	  wait until rising_edge(clk);
	  tx_rst <= '0';
	  wait until valid = '1';
	  tst_end<= TRUE;
     wait;
end process do_test;



end;
 
