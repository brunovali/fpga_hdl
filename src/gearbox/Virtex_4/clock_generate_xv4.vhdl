-------------------------------------------------------------------------------
-- Title      : clock synthesis
-- Project    : MSADC -- ICTP
-------------------------------------------------------------------------------
-- File       : clock_generate_xv4.vhd
-- Author     : Bruno Valinoti - bvalinot@ictp.it
-- Company    : ICTP-MLab -- INTI
-- Created    : 08/06/2020
-- Last update: 
-- Platform   : Xilinx ISE V14.7
-- Language   : VHDL '93
-------------------------------------------------------------------------------
-- Description: generates system clock from input clock
--
--              clk_80_i  :  80 MHz
--              dat_clk_o :  80 MHz
--              ser_clk_o : 320 MHz
-------------------------------------------------------------------------------
-- Copyright (c) 2023
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- entity
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

library UNISIM;
use UNISIM.vcomponents.all;


entity clock_generate_xv4 is
   port (
      clk_80_i   : in  std_logic;
      rst_i      : in  std_logic;
		clk_p_o    : out std_logic;
		clk_n_o    : out std_logic;
      ser_clk_o  : out std_logic;
      locked_o   : out std_logic
   );
end clock_generate_xv4;


-------------------------------------------------------------------------------
-- architecture
-------------------------------------------------------------------------------

architecture virtex4 of clock_generate_xv4 is

  -- DCM
  signal dcm1_clk0 : std_logic;
  signal dcm1_clkfx : std_logic;
  signal dcm1_clkfx180 : std_logic;
  signal dcm1_locked : std_logic;
  signal dcm1_reset : std_logic;

  -- BUFG
  signal clk_fb : std_logic;
  signal clk_320   : std_logic;

  -- clock forwarding
  signal o_ser_clk : std_logic;


begin 

  DCM_BASE_inst : DCM_BASE
  generic map (
    CLKDV_DIVIDE          => 2.0,     -- Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
                                      --   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
    CLKFX_DIVIDE          => 1,       -- Can be any interger from 1 to 32
    CLKFX_MULTIPLY        => 4,       -- Can be any integer from 2 to 32
    CLKIN_DIVIDE_BY_2     => false,    -- TRUE/FALSE to enable CLKIN divide by two feature
    CLKIN_PERIOD          => 12.5,    -- Specify period of input clock in ns from 1.25 to 1000.00  -
    CLKOUT_PHASE_SHIFT    => "NONE",  -- Specify phase shift mode of NONE or FIXED
    CLK_FEEDBACK          => "1X",    -- Specify clock feedback of NONE or 1X
    DCM_AUTOCALIBRATION   => true,    -- DCM calibrartion circuitry TRUE/FALSE
    DCM_PERFORMANCE_MODE  => "MAX_SPEED",  -- Can be MAX_SPEED or MAX_RANGE
    DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",  -- SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                                    --   an integer from 0 to 15
    DFS_FREQUENCY_MODE    => "HIGH",   -- LOW or HIGH frequency mode for frequency synthesis
    DLL_FREQUENCY_MODE    => "LOW",   -- LOW, HIGH, or HIGH_SER frequency mode for DLL
    DUTY_CYCLE_CORRECTION => true,    -- Duty cycle correction, TRUE or FALSE
    FACTORY_JF            => X"F0F0",  -- FACTORY JF Values Suggested to be set to X"F0F0"
    PHASE_SHIFT           => 0,       -- Amount of fixed phase shift from -255 to 1023
    STARTUP_WAIT          => false)   -- Delay configuration DONE until DCM LOCK, TRUE/FALSE
  port map (
    CLK0                  => dcm1_clk0,  -- 0 degree DCM CLK ouptput
    CLK180                => open,    -- 180 degree DCM CLK output
    CLK270                => open,    -- 270 degree DCM CLK output
    CLK2X                 => open,    -- 2X DCM CLK output
    CLK2X180              => open,    -- 2X, 180 degree DCM CLK out
    CLK90                 => open,    -- 90 degree DCM CLK output
    CLKDV                 => open,  -- Divided DCM CLK out (CLKDV_DIVIDE)
    CLKFX                 => dcm1_clkfx,  -- DCM CLK synthesis out (M/D)
    CLKFX180              => dcm1_clkfx180,    -- 180 degree CLK synthesis out
    LOCKED                => dcm1_locked,  -- DCM LOCK status output
    CLKFB                 => clk_fb,  -- DCM clock feedback
    CLKIN                 => clk_80_i,   -- Clock input (from IBUFG, BUFG or DCM)
    RST                   => dcm1_reset  -- DCM asynchronous reset input
  );

  DCM_Lock_Control : entity work.DCM_control
  port map(
    RESET         => not(rst_i),
    CLK_IN        => clk_80_i,
    LOCKED_IN     => dcm1_locked,
    DO2           => '0',
    DCM_RESET_OUT => dcm1_reset
  );

  feedback_80MHz : BUFG
  port map (
    O => clk_fb,     
    I => dcm1_clk0    
  );

  BUFG_320_clk : BUFG
  port map (
    O => clk_320,               -- Clock buffer output
    I => dcm1_clkfx             -- Clock buffer input
  );

  do_clock_fwd: ODDR
   generic map (
      DDR_CLK_EDGE => "OPPOSITE_EDGE",
      INIT         => '0',
      SRTYPE       => "SYNC"
   )
   port map (
      Q  => o_ser_clk,
      C  => clk_320,
      CE => '1',
      D1 => '1',
      D2 => '0',
      R  => '0',
      S  => '0'
   );
	
	do_diff_clk: OBUFDS
   generic map (
      CAPACITANCE => "DONT_CARE", -- "LOW", "NORMAL", "DONT_CARE" 
      IOSTANDARD => "LVDS_25")
   port map (
      O  => clk_p_o,     -- Diff_p output (connect directly to top-level port)
      OB => clk_n_o,     -- Diff_n output (connect directly to top-level port)
      I  => o_ser_clk      
   );
  

-- Output signal assignations
   locked_o<=dcm1_locked;
	--locked_o<=dcm1_reset;
   ser_clk_o <= clk_320;

end virtex4;
