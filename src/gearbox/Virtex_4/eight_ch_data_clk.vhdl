-------------------------------------------------------------------------------
-- Title      : Eigth Channels transmitter into 12 oserdes
-- Project    : MSADC
-------------------------------------------------------------------------------
-- File       : eight_ch_data_clk.vhdl
-- Author     : Bruno Valinoti
-- Company    : ICTP-Mlab  -- INTI 
-- Created    : 15/03/2023
-- Last update: 
-- Platform   : Xilinx ISE V14.7
-- Language   : VHDL '93
-------------------------------------------------------------------------------
-- Description:
-- 8 channels of 12 bits width @ 80MHz are transmitted using 12 OSERDES.
-- The clock scheme is also generated in this IP, providing the 320 MHz
-- clock required for transmitting together with the data outputs.
-------------------------------------------------------------------------------
-- Copyright (c) 2023
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.vcomponents.all;
library work;


---------------------------------------------------------------------------
--Entity
---------------------------------------------------------------------------

entity eight_ch_data_clk is
  port (
    clk_i       : in  std_logic;   -- this clock should be 80MHz 
    tx_rst_i    : in  std_logic;   -- this is the reset for the
    glbl_rst_i  : in  std_logic;
    sync_i      : in  std_logic;
    data_0_i    : in  std_logic_vector(11 downto 0);
    data_1_i    : in  std_logic_vector(11 downto 0);
    data_2_i    : in  std_logic_vector(11 downto 0);
    data_3_i    : in  std_logic_vector(11 downto 0);
    data_4_i    : in  std_logic_vector(11 downto 0);
    data_5_i    : in  std_logic_vector(11 downto 0);
    data_6_i    : in  std_logic_vector(11 downto 0);
    data_7_i    : in  std_logic_vector(11 downto 0);
	 --
	 valid_o     : out std_logic;
    --
    clk_p_o     : out std_logic;  -- This is the differential 320 MHz
    clk_n_o     : out std_logic;
	 -- DEBUG-->
	 clk_320_o   : out std_logic;
	 locked_o    : out std_logic;
    -- <-- DEBUG
    data_p_o    : out std_logic_vector(11 downto 0);
    data_n_o    : out std_logic_vector(11 downto 0)
    );
end eight_ch_data_clk;

architecture virtex4 of eight_ch_data_clk is
   signal valid     : std_logic;  -- Valid signal coming from piso_eight2twelve
	signal valid_fsm : std_logic;  -- Valid signal generated in the control FSM
   signal clk_320   : std_logic;   
   signal locked    : std_logic;
   signal sync      : std_logic_vector(1 downto 0);
   signal sync_pulse: std_logic;
	signal sync_fsm  : std_logic;
	signal sync_joined: std_logic;
   signal rst_p     : std_logic;
   signal rst_joined: std_logic;
	--
   type   state_t is (RST_S, LOCKED_S, SYN_S, WAIT_VALID_S, VALID_S);
   signal state, next_state : state_t := RST_S;
begin

rst_p <= not(glbl_rst_i);
rst_joined <= tx_rst_i; 

do_sync_pulse: process(clk_i) 
begin
   if rising_edge(clk_i) then
      sync(0) <= sync_i;
      sync(1) <= sync(0);
   end if;
end process do_sync_pulse;
sync_pulse <= not(sync(1)) and sync(0);
sync_joined <= sync_pulse or sync_fsm;

do_clocking: entity work.clock_generate_xv4
   port map(
      clk_80_i   => clk_i,
      rst_i      => rst_p,
      ser_clk_o  => clk_320,
		clk_p_o    => clk_p_o,
		clk_n_o    => clk_n_o,
      locked_o   => locked
   );

   clk_320_o <= clk_320;
do_8to12: entity work.piso_eight2twelve
    generic map ( 
        SYN_WORD   => x"01",
        SYN_CYCLES => 512,
        P_DAT_W    => 12
     )
    port map( 
        dat_clk_i  => clk_i,
        ser_clk_i  => clk_320,
        data_0_i   => data_0_i,
        data_1_i   => data_1_i,
        data_2_i   => data_2_i,
        data_3_i   => data_3_i,
        data_4_i   => data_4_i,
        data_5_i   => data_5_i,
        data_6_i   => data_6_i,
        data_7_i   => data_7_i,
        rst_i      => rst_joined,
        ena_i      => locked,
        sync_i     => sync_joined,	  
        valid_o    => valid,
        dat_p_o    => data_p_o,
        dat_n_o    => data_n_o
    );
	 valid_o <= valid and valid_fsm;
	 
	 ------------------------
	 --- FSM control
	 ------------------------
	SYNC_PROC: process (clk_i)
   begin
      if rising_edge(clk_i) then
         if (rst_p = '1') then
            state <= RST_S;
         else
            state <= next_state;
         end if;
      end if;
   end process;
	

   NEXT_STATE_DECODE: process (state, rst_joined, locked, sync_joined, valid)
   begin
      next_state <= state;  --default is to stay in current state
      case (state) is
         when RST_S =>
            if rst_p = '0' then
               next_state <= LOCKED_S;
            end if;
         when LOCKED_S =>
			   if locked = '1' then
               next_state <= SYN_S;
            end if;
         when SYN_S =>
            next_state <= WAIT_VALID_S;
         when WAIT_VALID_S =>
			   if valid = '1' then
				   next_state <= VALID_S;
            end if;
         when VALID_S =>
            if sync_pulse = '1' then 
               next_state <= SYN_S;
            else
               next_state <= VALID_S;
            end if;
         when others =>
            next_state <= RST_S;
      end case;
    end process;

   OUTPUT_DECODE: process (state, valid, sync_joined, rst_joined)
   begin
     case state is
        when RST_S =>
          sync_fsm  <= '0';
			 valid_fsm <= '0';
       when LOCKED_S =>
          sync_fsm  <= '0';
			 valid_fsm <= '0';
       when SYN_S =>
          sync_fsm  <= '1';
			 valid_fsm <= '0';
       when WAIT_VALID_S =>
		    sync_fsm  <= '0';
			 valid_fsm <= '0';
       when VALID_S =>
          sync_fsm  <= '0';
			 valid_fsm <= '1';
       when others =>
          sync_fsm  <= '0';
			 valid_fsm <= '0';
      end case;
   end process;
end architecture virtex4;

