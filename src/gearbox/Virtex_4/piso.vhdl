----------------------------------------------------------------------------------
-- Company: ICTP Mlab -- INTI
-- Engineer: Bruno Valinoti -- bvalinot@inti.gob.ar
-- 
-- Create Date: 03/15/2023 
-- Design Name: 
-- Module Name: piso - VIRTEX4
-- Project Name: 
-- Target Devices: Virtex4 -- MSADC
-- Tool Versions: 
-- Description: This IP 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity piso is
    generic ( 
        SYN_WORD  : std_logic_vector(7 downto 0) := x"01";
        SYN_CYCLES: integer := 512;
        P_DAT_W   : integer := 8
     );
    Port ( 
        dat_clk_i  : in  std_logic;
        ser_clk_i  : in  std_logic;
        data_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        rst_i      : in  std_logic;
        ena_i      : in  std_logic;
        sync_i     : in  std_logic;  
        valid_o    : out std_logic;
        data_o     : out std_logic
    );
end piso;

architecture virtex4 of piso is
   signal data       : std_logic_vector(P_DAT_W-1 downto 0);
   signal next_data  : std_logic_vector(P_DAT_W-1 downto 0);
   --
   signal count      : natural range 0 to SYN_CYCLES-1;
   signal next_count : natural range 0 to SYN_CYCLES-1;
   -- 
   type   state_t is (RST_S, N_SYN_S, SYN_S, VALID_S);
   signal state, next_state : state_t := RST_S;
   -- Architecture specific signals
   signal shiftout1_a, shiftout2_a: std_logic;
   signal shiftin1_a, shiftin2_a  : std_logic;
begin
    
   SYNC_PROC: process (dat_clk_i)
   begin
      if rising_edge(dat_clk_i) then
         if (rst_i = '1') then
            state <= RST_S;
            data  <= (others => '0');
            count <= 0;
         elsif ena_i = '1' then
            state <= next_state;
            data  <= next_data;
            count <= next_count;
         end if;
      end if;
  end process;
 
  OUTPUT_DECODE: process (state, count, data_i)
  begin
     case state is
        when RST_S =>
          valid_o <= '0';
          next_data <= (others => '0');
          next_count <= 0; 
       when N_SYN_S =>
          valid_o <= '0';
          next_data <= SYN_WORD;
          next_count <= 0;
       when SYN_S =>
          valid_o <= '0';
          next_data <= SYN_WORD;
          next_count <= count+1;
       when VALID_S =>
          valid_o <= '1';
          next_data <= data_i;
          next_count <= 0;
       when others =>
          valid_o <= '0';
      end case;
   end process;
 
   NEXT_STATE_DECODE: process (state, rst_i, count, sync_i)
   begin
      next_state <= state;  --default is to stay in current state
      case (state) is
         when RST_S =>
            if rst_i = '0' then
               next_state <= N_SYN_S;
            end if;
         when N_SYN_S =>
               next_state <= SYN_S;    
         when SYN_S =>
             if count > SYN_CYCLES - 2 then
                next_state <= VALID_S;
             else 
                next_state <= SYN_S;
             end if;
         when VALID_S =>
            if sync_i = '1' then 
               next_state <= SYN_S;
            else
               next_state <= VALID_S;
            end if;
         when others =>
            next_state <= RST_S;
      end case;
    end process;

-----------------------------------------------------------------------------------------
--- FROM THIS PART SHOULD BE REPLACED ACCORDING TO THE FPGA FAMILY  ------>
------------------------------------------------------------------------------------------   

--   OSERDESE3_inst : OSERDESE3
--   generic map (
--      DATA_WIDTH => 8,                 -- Parallel Data Width (4-8)
--      INIT       => '0',               -- Initialization value of the OSERDES flip-flops
--      IS_CLKDIV_INVERTED => '0',       -- Optional inversion for CLKDIV
--      IS_CLK_INVERTED    => '0',       -- Optional inversion for CLK
--      IS_RST_INVERTED    => '0',       -- Optional inversion for RST
--      SIM_DEVICE => "ULTRASCALE_PLUS"  -- Set the device version (ULTRASCALE, ULTRASCALE_PLUS,
                                       -- ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
--   )
--   port map (
--      OQ     => data_o,  -- 1-bit output: Serial Output Data
--      T_OUT  => open,       -- 1-bit output: 3-state control output to IOB
--      CLK    => ser_clk_i,   -- 1-bit input: High-speed clock
--      CLKDIV => dat_clk_i,   -- 1-bit input: Divided Clock
--      D      => data,  -- 8-bit input: Parallel Data Input
--      RST    => rst_i,        -- 1-bit input: Asynchronous Reset
--      T      => '0'         -- 1-bit input: Tristate input from fabric
--   );


   OSERDES_master_inst_A : OSERDES
   generic map (
      DATA_RATE_OQ => "DDR", -- Specify data rate to "DDR" or "SDR" 
      DATA_RATE_TQ => "DDR",
      DATA_WIDTH => 8, -- Specify data width - For DDR: 4,6,8, or 10 
                       -- For SDR or BUF: 2,3,4,5,6,7, or 8 
      INIT_OQ => '0',  -- INIT for Q1 register - '1' or '0' 
      INIT_TQ => '0',
      SERDES_MODE => "MASTER", --Set SERDES mode to "MASTER" or "SLAVE" 
      SRVAL_OQ => '0',
      SRVAL_TQ => '0',
      TRISTATE_WIDTH => 2
      ) 
   port map (
      OQ => data_o,     -- 1-bit output
      SHIFTOUT1 => open, -- 1-bit data expansion output
      SHIFTOUT2 => open, -- 1-bit data expansion output
      TQ => open,     -- 1-bit 3-state control output
      CLK => ser_clk_i,  -- 1-bit clock input
      CLKDIV => dat_clk_i,  -- 1-bit divided clock input

      D1 => data(0),    -- 1-bit parallel data input  
      D2 => data(1),    -- 1-bit parallel data input
      D3 => data(2),    -- 1-bit parallel data input
      D4 => data(3),    -- 1-bit parallel data input
      D5 => data(4),    -- 1-bit parallel data input
      D6 => data(5),    -- 1-bit parallel data input

      OCE => '1',  -- 1-bit clcok enable input
      REV => '0',  -- Must be tied to logic zero
      SHIFTIN1 => shiftout1_a, -- 1-bit data expansion input
      SHIFTIN2 => shiftout2_a, -- 1-bit data expansion input
      SR => rst_i,   -- 1-bit set/reset input
      T1 => '0',   -- 1-bit parallel 3-state input
      T2 => '0',   -- 1-bit parallel 3-state input
      T3 => '0',   -- 1-bit parallel 3-state input
      T4 => '0',   -- 1-bit parallel 3-state input
      TCE => '0'   -- 1-bit 3-state signal clock enable input
   );

   OSERDES_slave_inst_A : OSERDES
   generic map (
      DATA_RATE_OQ => "DDR", -- Specify data rate to "DDR" or "SDR" 
      DATA_RATE_TQ => "DDR",
      DATA_WIDTH => 8, -- Specify data width - For DDR: 4,6,8, or 10 
                       -- For SDR or BUF: 2,3,4,5,6,7, or 8 
      INIT_OQ => '0',  -- INIT for Q1 register - '1' or '0' 
      INIT_TQ => '0',
      SERDES_MODE => "SLAVE", --Set SERDES mode to "MASTER" or "SLAVE" 
      SRVAL_OQ => '0',
      SRVAL_TQ => '0',
      TRISTATE_WIDTH => 2
      )
   port map (
      OQ => open,    -- 1-bit output
      SHIFTOUT1 => shiftout1_a, -- 1-bit data expansion output
      SHIFTOUT2 => shiftout2_a, -- 1-bit data expansion output
      TQ => open,    -- 1-bit 3-state control output
      CLK => ser_clk_i,  -- 1-bit clock input
      CLKDIV => dat_clk_i,  -- 1-bit divided clock input
      D1 => '0',    -- 1-bit parallel data input
      D2 => '0',    -- 1-bit parallel data input
      D3 => data(6),    -- 1-bit parallel data input
      D4 => data(7),    -- 1-bit parallel data input
      D5 => '0',    -- 1-bit parallel data input
      D6 => '0',    -- 1-bit parallel data input
      OCE => '1',  -- 1-bit clcok enable input
      REV => '0',  -- Must be tied to logic zero
      SHIFTIN1 => shiftin1_a, -- 1-bit data expansion input
      SHIFTIN2 => shiftin2_a, -- 1-bit data expansion input
      SR => rst_i,   -- 1-bit set/reset input
      T1 => '0',   -- 1-bit parallel 3-state input
      T2 => '0',   -- 1-bit parallel 3-state input
      T3 => '0',   -- 1-bit parallel 3-state input
      T4 => '0',   -- 1-bit parallel 3-state input
      TCE => '0'  -- 1-bit 3-state signal clock enable input
   );

-----------------------------------------------------------------------------------------
---  <----- UP TO THIS PART
------------------------------------------------------------------------------------------   
end virtex4;

