----------------------------------------------------------------------------------
-- Company: ICTP - MLab & INTI
-- Engineer: Bruno Valinoti - bvalinot@ictp.it - valinoti@inti.gob.ar
-- 
-- Create Date: 03/10/2023 05:10:38 PM
-- Design Name: 
-- Module Name: PISO_8_combined - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity PISO_8_combined is
    generic ( 
        SYN_WORD  : std_logic_vector(7 downto 0) := x"01";
        SYN_CYCLES: integer := 512;
        P_DAT_W   : integer := 12
     );
    port ( 
        dat_clk_i  : in  std_logic;
        ser_clk_i  : in  std_logic;
        data_0_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_1_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_2_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_3_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_4_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_5_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_6_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        data_7_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        rst_i      : in  std_logic;
        ena_i      : in  std_logic;
        sync_i     : in  std_logic;  
        valid_o    : out std_logic;
        dat_p_o    : out std_logic_vector (11 downto 0);
        dat_n_o    : out std_logic_vector (11 downto 0)
    );
end PISO_8_combined;

architecture Behavioral of PISO_8_combined is
   signal data_lvds    : std_logic_vector(11 downto 0);
   signal valid_ch     : std_logic_vector(11 downto 0);
   --- all channels
   type all_ch_t is array(natural range<>) of std_logic_vector(P_DAT_W-1  downto 0);
   signal joined_data  : all_ch_t(7 downto 0);
   
   signal pre_sliced_data : std_logic_vector(8*12-1 downto 0);
   
   type all_serdes_t is array(natural range<>) of std_logic_vector(7 downto 0);
   signal joined_serdes  : all_serdes_t(11 downto 0);
begin

gen_serdes_data_joined: for j in 0 to 11 generate
   joined_serdes(j) <= pre_sliced_data(8*(j+1)-1 downto 8*j); 
end generate gen_serdes_data_joined;

gen_data_sliced: for j in 0 to 7 generate
   pre_sliced_data(12*(j+1)-1 downto 12*j) <= joined_data(j);
end generate gen_data_sliced;

   joined_data(0) <= data_0_i;
   joined_data(1) <= data_1_i;
   joined_data(2) <= data_2_i;
   joined_data(3) <= data_3_i;
   joined_data(4) <= data_4_i;
   joined_data(5) <= data_5_i;
   joined_data(6) <= data_6_i;
   joined_data(7) <= data_7_i;


gen_do_piso: for j in 0 to 11 generate
   do_se2diff: OBUFDS
   port map (
      I => data_lvds(j),  -- Buffer output
      O => dat_p_o(j),    -- Diff_p buffer output (connect directly to top-level port)
      OB => dat_n_o(j)   
   );
   
   PISO_inst: entity work.piso_usp
   generic map ( 
      SYN_WORD  => x"01",
      P_DAT_W   => 8,
      SYN_CYCLES => SYN_CYCLES
   )
   port map ( 
      dat_clk_i  => dat_clk_i,
      ser_clk_i  => ser_clk_i,
      data_i     => joined_serdes(j),
      rst_i      => rst_i,
      ena_i      => ena_i,
      sync_i     => sync_i,
      valid_o    => valid_ch(j),
      data_o     => data_lvds(j)
   );
   end generate gen_do_piso;
   
   valid_o <= valid_ch(0) and valid_ch(1) and valid_ch(2) and valid_ch(3) and valid_ch(4) and valid_ch(5) and valid_ch(6) and valid_ch(7); 
   
end Behavioral;
