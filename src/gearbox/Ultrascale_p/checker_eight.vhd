----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/10/2023 03:22:25 PM
-- Design Name: 
-- Module Name: checker_eight - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity checker_eight is
    generic ( P_DAT_W   : integer := 12 ); 
    port ( 
          clk_i   : in  std_logic;
          rst_i   : in  std_logic;
          sync_i  : in  std_logic;
          data_i  : in  std_logic_vector(P_DAT_W-1 downto 0);
          error_o : out std_logic
    );
end checker_eight;

architecture Behavioral of checker_eight is
   signal error_count_r    : integer range 0 to 65535;
   signal error_count_next : integer range 0 to 65535;
   signal error_r     : std_logic;
   ---
   signal data_r      : unsigned(P_DAT_W-1 downto 0);
   ---
   type   state_t is (RST_S, N_SYN_S, SYN_S, VALID_S);
   signal state, next_state : state_t := RST_S;
   ---
begin
   
   SYNC_PROC: process (clk_i)
   begin
      if rising_edge(clk_i) then
         if (rst_i = '1') then
            state <= RST_S;
            error_count_r  <= 0;
         else
            state  <= next_state;
            data_r <= unsigned(data_i);
            error_count_r <= error_count_next;
         end if;
      end if;
   end process;

   OUTPUT_DECODE: process (state, data_i, data_r)
   variable max_val : unsigned(P_DAT_W -1 downto 0):=(others => '1'); 
   begin
      error_count_next <= error_count_r;
      case state is
         when RST_S =>
            error_r <= '0';
         when N_SYN_S =>
            error_r <= '0';
            error_count_next <= 0;
         when SYN_S =>
            error_r <= '0';
         when VALID_S =>
           if data_r /= max_val then
              if unsigned(data_i) /= data_r + 1 then 
                 error_r <= '1';
                 error_count_next <= error_count_r + 1; 
              end if;
           end if;
         when others =>
            error_r <= '0';
            error_count_next <= 0;
      end case;
   end process;
   error_o <= error_r;

   NEXT_STATE_DECODE: process (state, rst_i, sync_i, data_i)
   begin
      next_state <= state;  --default is to stay in current state
      case (state) is
         when RST_S =>
            if rst_i = '0' then
               next_state <= N_SYN_S;
            end if;
         when N_SYN_S =>
            if  sync_i = '1' then
               next_state <= SYN_S;
            end if;         
         when SYN_S =>
            if data_i /= x"010" or data_i /= x"101" then 
               next_state <= VALID_S;
            end if;
         when VALID_S =>
            next_state <= VALID_S;
         when others =>
            next_state <= RST_S;
      end case;
   end process;



end Behavioral;
