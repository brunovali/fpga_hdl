----------------------------------------------------------------------------------
-- Company: ICTP MLab -- INTI 
-- Engineer: Bruno Valinoti - bvalinot@ictp.it
-- 
-- Create Date: 02/10/2023 05:32:43 PM
-- Design Name: 
-- Module Name: sipo_USP_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library std;
use std.textio.all;
library UNISIM;
use UNISIM.VComponents.all;

entity sipo_USP_tb is
--  Port ( );
end sipo_USP_tb;

architecture Behavioral of sipo_USP_tb is
   signal clk_fast     : std_logic;
   signal clk_fast_dly : std_logic;
   signal n_clk_fast   : std_logic;

   signal clk_data     : std_logic;
   signal clk_data_dly : std_logic;

   signal rst          : std_logic := '1';
   signal rst_data     : std_logic := '1';
   signal data_oser    : std_logic_vector(7 downto 0);
   signal data_out     : std_logic_vector(7 downto 0);
   signal data_lvds    : std_logic := '0';
   signal bitslip      : std_logic := '0';
   signal bitslip_done : std_logic := '0';
   
   signal sync_ch      : std_logic := '0';
   signal valid        : std_logic := '0';
   signal tst_flg      : std_logic := '0';
  
   signal stop         : boolean := FALSE;
   constant N_SAMPL    : integer := 1; 
   type t_int_array is array( integer range <> ) of integer;
   
   --- TESTING OOC
   signal clk_40_t, clk_40_r  : std_logic;
   signal clk_80_t  : std_logic;
   signal clk_320_t : std_logic;
   signal clk_320_t_n : std_logic;
   signal locked_t  : std_logic;
   signal data_lvds_n : std_logic;
   signal ena         : std_logic := '1';
   
   signal sync      : std_logic := '0';
   signal data_0    : std_logic_vector(11 downto 0);
   signal data_1    : std_logic_vector(11 downto 0);
   signal data_2    : std_logic_vector(11 downto 0);
   signal data_3    : std_logic_vector(11 downto 0);
   signal data_4    : std_logic_vector(11 downto 0);
   signal data_5    : std_logic_vector(11 downto 0);
   signal data_6    : std_logic_vector(11 downto 0);
   signal data_7    : std_logic_vector(11 downto 0);
   
   signal dat_p     : std_logic_vector(11 downto 0);
   signal dat_n     : std_logic_vector(11 downto 0);
begin


do_fast_clock : entity work.Clock
   generic map (
      FREQUENCY  => 320e6
   )
   port map(
      clk_o  => clk_fast,
      rst_o  => rst,
      stop_i => stop
   );
   n_clk_fast <= not(clk_fast);

   
do_clock_delay: process
begin
   wait until rising_edge(clk_fast);
   wait for 100 ps;
   clk_fast_dly <= clk_fast;
   clk_data_dly <= clk_data;
   wait until falling_edge(clk_fast);
   wait for 100 ps;
   clk_fast_dly <= clk_fast;
   clk_data_dly <= clk_data;
  
end process do_clock_delay; 
   
--do_data_clock : entity work.Clock
--   generic map (
--      FREQUENCY  => 80e6
--   )
--   port map(
--      clk_o  => clk_data,
--      rst_o  => rst_data,
--      stop_i => stop
--   );

   clk80_inst : BUFGCE_DIV
   generic map (
      BUFGCE_DIVIDE => 4,     -- 1-8
      -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
      IS_CE_INVERTED => '0',  -- Optional inversion for CE
      IS_CLR_INVERTED => '0', -- Optional inversion for CLR
      IS_I_INVERTED => '0'    -- Optional inversion for I
   )
   port map (
      O => clk_data,     -- 1-bit output: Buffer
      CE => '1',   -- 1-bit input: Buffer enable
      CLR => rst, -- 1-bit input: Asynchronous clear
      I => clk_fast      -- 1-bit input: Buffer
   );


SIPO_inst: entity work.sipo_USP
   generic map ( 
--      I_DAT_W   => 1,
      SYN_WORD  => x"01",
      P_DAT_W   => 8
   )
   port map ( 
      dat_clk_i  => clk_80_t,
      ser_clk_i  => clk_320_t,
      n_ser_clk_i=> clk_320_t_n,
      data_i     => data_lvds,
      rst_i      => rst,
      bitslip_i  => bitslip,
      syn_o      => sync_ch,
      valid_o    => valid,
      data_o     => data_out
   );
clk_320_t_n <= not(clk_320_t);

   OSERDESE3_inst : OSERDESE3
   generic map (
      DATA_WIDTH => 8,                 -- Parallel Data Width (4-8)
      INIT       => '0',               -- Initialization value of the OSERDES flip-flops
      IS_CLKDIV_INVERTED => '0',       -- Optional inversion for CLKDIV
      IS_CLK_INVERTED    => '0',       -- Optional inversion for CLK
      IS_RST_INVERTED    => '0',       -- Optional inversion for RST
      SIM_DEVICE => "ULTRASCALE_PLUS"  -- Set the device version (ULTRASCALE, ULTRASCALE_PLUS,
                                       -- ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
   )
   port map (
      OQ     => data_lvds,  -- 1-bit output: Serial Output Data
      T_OUT  => open,       -- 1-bit output: 3-state control output to IOB
      CLK    => clk_320_t,   -- 1-bit input: High-speed clock
      CLKDIV => clk_80_t,   -- 1-bit input: Divided Clock
      D      => data_oser,  -- 8-bit input: Parallel Data Input
      RST    => rst,        -- 1-bit input: Asynchronous Reset
      T      => '0'         -- 1-bit input: Tristate input from fabric
   );




   test_dut : process
      file cha_file   : text open read_mode is "ecal_pulse.csv";--"salida.csv"; -- "ADCa.dat";
      variable a_row  : line;
      variable a_read : t_int_array(1 to N_SAMPL);
      begin
         tst_flg <= '0';
         ena <= '0';
         wait until rst = '0';
--         -- synchronize serdes bitslip
         ena <= '1';
         data_oser <= x"01";
         tst_flg <= '1';
         wait for 200 ns;
         tst_flg <= '0'; 
         wait until rising_edge(clk_data);
         sync <= '1';
         wait until rising_edge(clk_data);
         sync <= '0';  
         tst_flg <= '1';
         while data_out /= x"01" loop
            bitslip <= '1';
            wait for 1 ns;
            wait until  rising_edge(clk_data);
            bitslip <= '0';
            wait for 1 ns;
            wait until  rising_edge(clk_data);            
           wait for 500 ns;
         end loop;
         tst_flg <= '0';
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk_data);       
            for i in 0 to 2048 loop
               data_oser <= std_logic_vector(to_unsigned(i,8));
               data_0 <= std_logic_vector(to_unsigned(i,12));
               data_1 <= std_logic_vector(to_unsigned(i+1,12));
               data_2 <= std_logic_vector(to_unsigned(i+2,12));
               data_3 <= std_logic_vector(to_unsigned(i+3,12));
               data_4 <= std_logic_vector(to_unsigned(i+4,12));
               data_5 <= std_logic_vector(to_unsigned(i+5,12));
               data_6 <= std_logic_vector(to_unsigned(i+6,12));
               data_7 <= std_logic_vector(to_unsigned(i+7,12));
               wait for 1 ns;
               wait until rising_edge(clk_data);
            end loop;
         stop <= TRUE;
         wait;
   end process test_dut;

-----Out Of Context testing

do_40_clock : entity work.Clock
   generic map (
      FREQUENCY  => 40e6
   )
   port map(
      clk_o  => clk_40_t,
      rst_o  => open,
      stop_i => stop
   );
  clk_inst : entity work.clock_manager
  port map (
     clk40_i     => clk_40_t,
     rst_i       => rst,
     clk320_p_i  => clk_fast,
     clk320_n_i  => n_clk_fast, 
     clk_40M_p_o => clk_40_r,   
     clk_40M_n_o => open,
     clk_320_o   => clk_320_t,
     clk_80_o    => clk_80_t,
     locked_o    => locked_t
     );


do_top: entity work.top_fecca
   port map(
      clk_40_i => clk_40_t,
      rst_i   => rst,
      rst_btn_i => rst,
      rst_fifo_i => rst,
      --
      test_i  => rst,    -- enables test mode (fake data generator)
      ena_i   => ena,
      leds_o  => open,
      error_o => open, 
      valid_o => open,  
      data_clk80_o => open,
      --- DATA channels
      data_0_o  => open,
      data_1_o  => open,
      data_2_o  => open,
      data_3_o  => open,
      data_4_o  => open,
      data_5_o  => open,
      data_6_o  => open,
      data_7_o  => open,
      -------------------------------------------------------------------------
      -- MSADC Board side
      -------------------------------------------------------------------------
      -- Channel A
      dat_p_i   => dat_p, --data_lvds & data_lvds & data_lvds & data_lvds &data_lvds & data_lvds &data_lvds & data_lvds & data_lvds & data_lvds & data_lvds & data_lvds,
      dat_n_i   => dat_n, --data_lvds_n & data_lvds_n & data_lvds_n & data_lvds_n &data_lvds_n & data_lvds_n &data_lvds_n & data_lvds_n & data_lvds_n & data_lvds_n & data_lvds_n & data_lvds_n,
      -- Clock input synchronic with data input (320 MHz)
      msadc_clk320_p_i   => clk_320_t,
      msadc_clk320_n_i   => clk_320_t_n,
      -- Clock output 40MHz for Data Sampling
      msadc_clk40_p_o  => open,
      msadc_clk40_n_o  => open,
      -- MSADC Control signals
      msadc_rst_o      => open
      );
      data_lvds_n <= not (data_lvds);
  
  
  do_piso: entity work.PISO_8_combined
    generic map ( 
        SYN_WORD   => x"01",
        SYN_CYCLES => 512,
        P_DAT_W    => 12
     )
    port map ( 
        dat_clk_i => clk_80_t,
        ser_clk_i => clk_320_t,
        data_0_i  => data_0,
        data_1_i  => data_1,
        data_2_i  => data_2,
        data_3_i  => data_3,
        data_4_i  => data_4,
        data_5_i  => data_5,
        data_6_i  => data_6,
        data_7_i  => data_7,
        rst_i     => rst,
        ena_i     => ena,
        sync_i    => sync,  
        valid_o   => open,
        dat_p_o   => dat_p, -- : out std_logic_vector (11 downto 0);
        dat_n_o   => dat_n --: out std_logic_vector (11 downto 0)
    );
end Behavioral;
