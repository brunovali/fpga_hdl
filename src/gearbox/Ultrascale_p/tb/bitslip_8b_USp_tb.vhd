----------------------------------------------------------------------------------
-- Company: ICTP MLab -- INTI
-- Engineer: Bruno Valinoti
-- 
-- Create Date: 02/16/2023 04:55:58 PM
-- Design Name: 
-- Module Name: bitslip_8b_USp_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library std;
use std.textio.all;
library UNISIM;
use UNISIM.VComponents.all;

entity bitslip_8b_USp_tb is
--  Port ( );
end bitslip_8b_USp_tb;

architecture Behavioral of bitslip_8b_USp_tb is
   signal clk          : std_logic;
   signal rst          : std_logic := '1';
   signal rst_tst      : std_logic := '0';
   signal rst_dut      : std_logic := '0';
   signal data1,data0  : std_logic_vector( 7 downto 0):=(others => '0');
   signal data3,data2  : std_logic_vector( 7 downto 0):=(others => '0');
   signal data5,data4  : std_logic_vector( 7 downto 0):=(others => '0');
   
   signal data         : std_logic_vector( 7 downto 0);
   signal data_inv     : std_logic_vector( 7 downto 0);
   signal data_aligned : std_logic_vector( 7 downto 0);
   signal init         : std_logic := '0';
   
   signal sync_ch      : std_logic := '0';
   signal op_flag      : std_logic := '0';
  
   signal stop         : boolean := FALSE;
   constant N_SAMPL    : integer := 1; 
   type t_int_array is array( integer range <> ) of integer;
   
begin

do_data_clock : entity work.Clock
   generic map (
      FREQUENCY  => 80e6
   )
   port map(
      clk_o  => clk,
      rst_o  => rst,
      stop_i => stop
   );

rst_dut <= rst or rst_tst;
 
inst_dut: entity work.bitslip_8b_USp
    port map( 
       clk_i      => clk,
       rst_i      => rst_dut,
       init_i     => init,
       data_i     => data_inv,
       init_val_i => x"01",
       sync_o     => sync_ch,
       data_o     => data_aligned
       );
       
      data_inv <= not(data);
  test_dut : process
      file cha_file   : text open read_mode is "ecal_pulse.csv";--"salida.csv"; -- "ADCa.dat";
      variable a_row  : line;
      variable a_read : t_int_array(1 to N_SAMPL);
      begin
         wait until rst = '0';

         data <= x"01";
         wait for 100 ns;
         wait until rising_edge(clk);  
         init <= '1';
         wait for 1 ps;
         wait until rising_edge(clk);            
         init <= '0';
         wait until rising_edge(clk);
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 100 ns;
         end loop;
         
         file_open(cha_file, "ecal_pulse.csv", READ_MODE); ---"ADCa.dat", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            for i in 1 to N_SAMPL loop
               read(a_row,a_read(i));
               data <= std_logic_vector(to_unsigned(a_read(i),8));
               data0 <= data;
               wait for 1 ns;
               wait until rising_edge(clk);
               wait for 1 ns;
            assert data_aligned = data1 report "error on bitslip 0, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data1)))
            severity failure;
            end loop;
         end loop;
         op_flag <= '0';
         file_close(cha_file);
---- RESET TEST
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity failure;
-----------------------------------------        
---- SHIFT data in one position ahaead
-----------------------------------------
         data <= x"02";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(6 downto 0) & data0(7);
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"50" and data_aligned /= x"01"  then 
               assert data_aligned = data4 report "error on bitslip 1, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);

-----------------------------------------        
---- SHIFT data in two positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"04";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(5 downto 0) & data0(7 downto 6); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"02"  then 
               assert data_aligned = data4 report "error on bitslip 2, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);


-----------------------------------------        
---- SHIFT data in three positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"08";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(4 downto 0) & data0(7 downto 5); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"02"  then 
               assert data_aligned = data3 report "error on bitslip 3, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);


-----------------------------------------        
---- SHIFT data in four positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"10";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(3 downto 0) & data0(7 downto 4); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"02"  then 
               assert data_aligned = data3 report "error on bitslip 4, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);

-----------------------------------------        
---- SHIFT data in five positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"20";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(2 downto 0) & data0(7 downto 3); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"02"  then 
               assert data_aligned = data3 report "error on bitslip 5, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);

-----------------------------------------        
---- SHIFT data in six positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"40";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(1 downto 0) & data0(7 downto 2); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"02"  then 
               assert data_aligned = data3 report "error on bitslip 6, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);

-----------------------------------------        
---- SHIFT data in seven positions ahaead
-----------------------------------------
---- RESET 
         rst_tst <= '1';
         wait until rising_edge(clk);
         rst_tst <= '0';
         wait until rising_edge(clk);
         wait for 1 ps;
         assert data_aligned = x"00" report "error after reset" severity error;
--- Prepare data
         data <= x"80";
         wait for 100 ns;
         wait until rising_edge(clk);  
         while data_aligned /= x"01" loop
            init <= '1';
            wait for 1 ps;
            wait until rising_edge(clk);            
            init <= '0';
            wait for 1 ps;
            wait until rising_edge(clk);            
            wait for 400 ns;
         end loop;
         file_open(cha_file, "ecal_pulse.csv", READ_MODE);
         wait until rising_edge(clk);       
         -- signal read from FILE
         op_flag <= '1';
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            read(a_row,a_read(1));
            data0 <= std_logic_vector(to_unsigned(a_read(1),8));
            data <= data1(0) & data0(7 downto 1); --- NEW SHIFTED DATA
            wait for 1 ns;
            wait until rising_edge(clk);
            if data_aligned /= x"01" and data_aligned /= x"00"  then 
               assert data_aligned = data3 report "error on bitslip 7, the value: is " & integer'image(to_integer(unsigned(data_aligned))) &" but must be: " & integer'image(to_integer(unsigned(data3)))
               severity failure;
           end if;
         end loop;
         op_flag <= '0';
         file_close(cha_file);
           
           
           
       
         stop <= TRUE;
         wait;
   end process test_dut;
 
bit_delay: process (clk)
begin
   if rising_edge(clk) then 
      data1 <= data0;
      data2 <= data1;
      data3 <= data2;
      data4 <= data3;
      data5 <= data4;
   end if;
end process bit_delay; 
 
end Behavioral;
