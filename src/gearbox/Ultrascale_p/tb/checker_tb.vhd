----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/20/2023 02:14:52 PM
-- Design Name: 
-- Module Name: checker_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity checker_tb is
--  Port ( );
end checker_tb;

architecture Behavioral of checker_tb is
   signal clk_data     : std_logic;
   signal rst          : std_logic := '1';
   signal valid        : std_logic := '0';
   signal stop         : boolean := FALSE;
   signal ena         : std_logic := '1';
   
   signal data_0    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_1    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_2    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_3    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_4    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_5    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_6    : std_logic_vector(11 downto 0):= (others => '0');
   signal data_7    : std_logic_vector(11 downto 0):= (others => '0');

   signal error_ramp : std_logic;
   signal error_all  : std_logic_vector(7 downto 0);
begin

do_clock : entity work.Clock
   generic map (
      FREQUENCY  => 80e6
   )
   port map(
      clk_o  => clk_data,
      rst_o  => rst,
      stop_i => stop
   );
  
 do_checker: entity work.checker_eight
    port map (
        clk_i   => clk_data,
        rst_i   => rst,
        sync_i  => valid,
        data_i  => data_0,
        error_o => error_ramp
    );
  
 do_checker_all: entity work.checker_all
    port map (
        clk_i   => clk_data,
        rst_i   => rst,
        sync_i  => valid,
        data_0_i  => data_0,
        data_1_i  => data_1,
        data_2_i  => data_2,
        data_3_i  => data_3,
        data_4_i  => data_4,
        data_5_i  => data_5,
        data_6_i  => data_6,
        data_7_i  => data_7,
        error_o => error_all
    );  
  
   test_dut : process

      begin
         ena <= '0';
         wait until rst = '0';
         wait for 200 ns;
         wait until rising_edge(clk_data);
         valid <= '1';      
            for i in 0 to 7000 loop
               data_0 <= std_logic_vector(to_unsigned(i  ,12));
               data_1 <= std_logic_vector(to_unsigned(i+1,12));
               data_2 <= std_logic_vector(to_unsigned(i+2,12));
               data_3 <= std_logic_vector(to_unsigned(i+3,12));
               data_4 <= std_logic_vector(to_unsigned(i+4,12));
               data_5 <= std_logic_vector(to_unsigned(i+5,12));
               data_6 <= std_logic_vector(to_unsigned(i+6,12));
               data_7 <= std_logic_vector(to_unsigned(i+7,12));
               wait for 1 ns;
               wait until rising_edge(clk_data);
            end loop;
         wait until rising_edge(clk_data);
         ---- Generate errors
         data_0 <= std_logic_vector(to_unsigned(3,12));
         wait until rising_edge(clk_data);
         data_0 <= std_logic_vector(to_unsigned(4,12));
         wait until rising_edge(clk_data);
         data_0 <= std_logic_vector(to_unsigned(5,12));
         wait until rising_edge(clk_data);
         data_0 <= std_logic_vector(to_unsigned(4,12));
         wait for 400 ns;
         wait until rising_edge(clk_data);
         stop <= TRUE;
         wait;
   end process test_dut;


end Behavioral;
