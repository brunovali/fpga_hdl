----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/20/2023 03:51:38 PM
-- Design Name: 
-- Module Name: checker_all - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity checker_all is
    generic ( P_DAT_W   : integer := 12 ); 
    port ( 
          clk_i    : in  std_logic;
          rst_i    : in  std_logic;
          sync_i   : in  std_logic;
          data_0_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_1_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_2_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_3_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_4_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_5_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_6_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          data_7_i : in  std_logic_vector(P_DAT_W-1 downto 0);
          error_o  : out std_logic_vector(7 downto 0)
    );
end checker_all;

architecture Behavioral of checker_all is
   signal error_r  : std_logic_vector(7 downto 0);
   
   type all_ch_t is array(natural range<>) of std_logic_vector(P_DAT_W-1  downto 0);
   signal joined_data  : all_ch_t(7 downto 0);
begin
   joined_data(0) <= data_0_i;
   joined_data(1) <= data_1_i;
   joined_data(2) <= data_2_i;
   joined_data(3) <= data_3_i;
   joined_data(4) <= data_4_i;
   joined_data(5) <= data_5_i;
   joined_data(6) <= data_6_i;
   joined_data(7) <= data_7_i;

   do_gen_all: for j in 0 to 7 generate
       do_checker: entity work.checker_eight
       port map (
           clk_i   => clk_i,
           rst_i   => rst_i,
           sync_i  => sync_i,
           data_i  => joined_data(j),
           error_o => error_r(j)
       );
   end generate do_gen_all;
   error_o <= error_r;
end Behavioral;
