----------------------------------------------------------------------------------
-- Company: ICTP MLab -- INFN TS -- INTI 
-- Engineer: Bruno Valinoti - bvalinot@ictp.it
-- 
-- Create Date: 03/07/2023 01:55:58 PM
-- Design Name: 
-- Module Name: clock_manager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity clock_manager is
   port (
      clk40_i     :  in std_logic;   -- Here a 40MHz clock from the PS is received, for forwarding to the MSADC
      rst_i       :  in std_logic;   -- From system reset
      clk320_p_i  :  in std_logic;   -- Here a 320MHz differential clock is received
      clk320_n_i  :  in std_logic;

      clk_40M_p_o : out std_logic;   -- This is the clock to forward to the MSADC
      clk_40M_n_o : out std_logic;
      clk_320_o   : out std_logic;
      clk_80_o    : out std_logic;    -- This is the 80MHz clock synchronous with the data coming from MSADC
      locked_o    : out std_logic
   );
end clock_manager;

architecture Behavioral of clock_manager is
  signal clk_40   : std_logic;
  signal clk320_1, clk320_2, clk320_3, clk320_4, clk320_6 : std_logic;  -- See UG572 (V1.10.2), page 57
  signal clk_320  : std_logic;                                          -- In the scheme of figure 3-9 is clk320_5
  signal clk_80, clk_80_1   : std_logic;
begin
---------------------------
--- 40 MHz clk forwarding
---------------------------

foward_clk40_inst: ODDRE1
   generic map (
      IS_C_INVERTED => '0',            -- Optional inversion for C
      IS_D1_INVERTED => '0',           -- Unsupported, do not use
      IS_D2_INVERTED => '0',           -- Unsupported, do not use
      SIM_DEVICE => "ULTRASCALE_PLUS", -- Set the device version (ULTRASCALE, ULTRASCALE_PLUS,
                                       -- ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
      SRVAL => '0'  
   )
   port map (
      Q  => clk_40,
      C  => clk40_i,
      D1 => '1',
      D2 => '0',
      SR => '0'
      );
   
diff_output_40_inst: OBUFDS
   port map (
      I  => clk_40,
      O  => clk_40M_p_o,
      OB => clk_40M_n_o
   );
   
---------------------------
--- 320 MHz clk 
---------------------------
  ibufgds_clk_320_inst : IBUFDS
   port map (
      I  => clk320_p_i,
      IB => clk320_n_i,
      O  => clk320_1
   );
   
  i_bufg_320 : BUFG
  port map (
     I => clk320_1,
     O => clk320_2
  ); 

  fb_bufg_320 : BUFG
  port map (
     I => clk320_6,
     O => clk320_3
  ); 
  
   MMCME3_BASE_inst : MMCME3_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",   -- Jitter programming (HIGH, LOW, OPTIMIZED)
      CLKFBOUT_MULT_F => 8.0,     -- Multiply value for all CLKOUT (2.000-64.000)
      CLKFBOUT_PHASE => 0.0,      -- Phase offset in degrees of CLKFB (-360.000-360.000)
      CLKIN1_PERIOD => 3.125,       -- Input clock period in ns units, ps resolution (i.e., 33.333 is 30 MHz).
      CLKOUT0_DIVIDE_F => 4.0,    -- Divide amount for CLKOUT0 (1.000-128.000)
      -- CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
      CLKOUT0_DUTY_CYCLE => 0.5,
      CLKOUT1_DUTY_CYCLE => 0.5,
      CLKOUT2_DUTY_CYCLE => 0.5,
      CLKOUT3_DUTY_CYCLE => 0.5,
      CLKOUT4_DUTY_CYCLE => 0.5,
      CLKOUT5_DUTY_CYCLE => 0.5,
      CLKOUT6_DUTY_CYCLE => 0.5,
      -- CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
      CLKOUT0_PHASE => 0.0,
      CLKOUT1_PHASE => 0.0,
      CLKOUT2_PHASE => 0.0,
      CLKOUT3_PHASE => 0.0,
      CLKOUT4_PHASE => 0.0,
      CLKOUT5_PHASE => 0.0,
      CLKOUT6_PHASE => 0.0,
      -- CLKOUT1_DIVIDE - CLKOUT6_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT1_DIVIDE => 16,
      CLKOUT2_DIVIDE => 1,
      CLKOUT3_DIVIDE => 1,
      CLKOUT4_DIVIDE => 1,
      CLKOUT5_DIVIDE => 1,
      CLKOUT6_DIVIDE => 1,
      CLKOUT4_CASCADE => "FALSE", -- Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
      DIVCLK_DIVIDE => 2,         -- Master division value (1-106)
      -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
      IS_CLKFBIN_INVERTED => '0', -- Optional inversion for CLKFBIN
      IS_CLKIN1_INVERTED => '0',  -- Optional inversion for CLKIN1
      IS_PWRDWN_INVERTED => '0',  -- Optional inversion for PWRDWN
      IS_RST_INVERTED => '0',     -- Optional inversion for RST
      REF_JITTER1 => 0.0,         -- Reference input jitter in UI (0.000-0.999)
      STARTUP_WAIT => "FALSE"     -- Delays DONE until MMCM is locked (FALSE, TRUE)
   )
   port map (
      -- Clock Outputs outputs: User configurable clock outputs
      CLKOUT0 => clk320_4,     -- 1-bit output: CLKOUT0
      CLKOUT0B => open,   -- 1-bit output: Inverted CLKOUT0
      CLKOUT1 => clk_80_1,     -- 1-bit output: CLKOUT1
      CLKOUT1B => open,   -- 1-bit output: Inverted CLKOUT1
      CLKOUT2 => open,     -- 1-bit output: CLKOUT2
      CLKOUT2B => open,   -- 1-bit output: Inverted CLKOUT2
      CLKOUT3 => open,     -- 1-bit output: CLKOUT3
      CLKOUT3B => open,   -- 1-bit output: Inverted CLKOUT3
      CLKOUT4 => open,     -- 1-bit output: CLKOUT4
      CLKOUT5 => open,     -- 1-bit output: CLKOUT5
      CLKOUT6 => open,     -- 1-bit output: CLKOUT6
      -- Feedback outputs: Clock feedback ports
      CLKFBOUT => clk320_6,   -- 1-bit output: Feedback clock
      CLKFBOUTB => open, -- 1-bit output: Inverted CLKFBOUT
      -- Status Ports outputs: MMCM status ports
      LOCKED => locked_o,       -- 1-bit output: LOCK
      -- Clock Inputs inputs: Clock input
      CLKIN1 => clk320_1,       -- 1-bit input: Clock
      -- Control Ports inputs: MMCM control ports
      PWRDWN => '0',       -- 1-bit input: Power-down
      RST => rst_i,             -- 1-bit input: Reset
      -- Feedback inputs: Clock feedback ports
      CLKFBIN => clk320_3      -- 1-bit input: Feedback clock
   );


   clk320_clktree_inst: BUFG
   port map (
      I   => clk320_4,
      O   => clk_320
   );
   clk_320_o <= clk_320;
   
   
--   clk80_clktree_inst: BUFG
--   port map (
--      I   => clk_80_1,
--      O   => clk_80
--   );
   
   
   BUFGCE_DIV_inst : BUFGCE_DIV
   generic map (
      BUFGCE_DIVIDE => 4,     -- 1-8
      -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
      IS_CE_INVERTED => '0',  -- Optional inversion for CE
      IS_CLR_INVERTED => '0', -- Optional inversion for CLR
      IS_I_INVERTED => '0'    -- Optional inversion for I
   )
   port map (
      O => clk_80,     -- 1-bit output: Buffer
      CE => '1',   -- 1-bit input: Buffer enable
      CLR => rst_i, -- 1-bit input: Asynchronous clear
      I => clk320_4      -- 1-bit input: Buffer
   );
   
   clk_80_o <= clk_80;
end Behavioral;
