----------------------------------------------------------------------------------
-- Company: ICTP Mlab -- INTI
-- Engineer: Bruno Valinoti -- bvalinot@inti.gob.ar
-- 
-- Create Date: 03/13/2023 03:59:37 PM
-- Design Name: 
-- Module Name: piso_usp - ultrascale_plus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity piso_usp is
    generic ( 
        SYN_WORD  : std_logic_vector(7 downto 0) := x"01";
        SYN_CYCLES: integer := 512;
        P_DAT_W   : integer := 8
     );
    Port ( 
        dat_clk_i  : in  std_logic;
        ser_clk_i  : in  std_logic;
        data_i     : in  std_logic_vector (P_DAT_W-1 downto 0);
        rst_i      : in  std_logic;
        ena_i      : in  std_logic;
        sync_i     : in  std_logic;  
        valid_o    : out std_logic;
        data_o     : out std_logic
    );
end piso_usp;

architecture ultrascale_plus of piso_usp is
   signal data       : std_logic_vector(P_DAT_W-1 downto 0);
   signal next_data  : std_logic_vector(P_DAT_W-1 downto 0);
   --
   signal count      : natural range 0 to SYN_CYCLES-1;
   signal next_count : natural range 0 to SYN_CYCLES-1;
   -- 
   type   state_t is (RST_S, N_SYN_S, SYN_S, VALID_S);
   signal state, next_state : state_t := RST_S;
begin
    
   SYNC_PROC: process (dat_clk_i)
   begin
      if rising_edge(dat_clk_i) then
         if (rst_i = '1') then
            state <= RST_S;
            data  <= (others => '0');
            count <= 0;
         else
            state <= next_state;
            data  <= next_data;
            count <= next_count;
         end if;
      end if;
  end process;
 
  OUTPUT_DECODE: process (state, count, data_i)
  begin
     case state is
        when RST_S =>
          valid_o <= '0';
          next_data <= (others => '0');
          next_count <= 0; 
       when N_SYN_S =>
          valid_o <= '0';
          next_data <= SYN_WORD;
          next_count <= 0;
       when SYN_S =>
          valid_o <= '0';
          next_data <= SYN_WORD;
          next_count <= count+1;
       when VALID_S =>
          valid_o <= '1';
          next_data <= data_i;
          next_count <= 0;
       when others =>
          valid_o <= '0';
      end case;
   end process;
 
   NEXT_STATE_DECODE: process (state, rst_i, count, sync_i)
   begin
      next_state <= state;  --default is to stay in current state
      case (state) is
         when RST_S =>
            if rst_i = '0' then
               next_state <= N_SYN_S;
            end if;
         when N_SYN_S =>
               next_state <= SYN_S;    
         when SYN_S =>
             if count > SYN_CYCLES - 2 then
                next_state <= VALID_S;
             else 
                next_state <= SYN_S;
             end if;
         when VALID_S =>
            if sync_i = '1' then 
               next_state <= SYN_S;
            else
               next_state <= VALID_S;
            end if;
         when others =>
            next_state <= RST_S;
      end case;
    end process;

-----------------------------------------------------------------------------------------
--- FROM THIS PART SHOULD BE REPLACED ACCORDING TO THE FPGA FAMILY  ------>
------------------------------------------------------------------------------------------   

   OSERDESE3_inst : OSERDESE3
   generic map (
      DATA_WIDTH => 8,                 -- Parallel Data Width (4-8)
      INIT       => '0',               -- Initialization value of the OSERDES flip-flops
      IS_CLKDIV_INVERTED => '0',       -- Optional inversion for CLKDIV
      IS_CLK_INVERTED    => '0',       -- Optional inversion for CLK
      IS_RST_INVERTED    => '0',       -- Optional inversion for RST
      SIM_DEVICE => "ULTRASCALE_PLUS"  -- Set the device version (ULTRASCALE, ULTRASCALE_PLUS,
                                       -- ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
   )
   port map (
      OQ     => data_o,  -- 1-bit output: Serial Output Data
      T_OUT  => open,       -- 1-bit output: 3-state control output to IOB
      CLK    => ser_clk_i,   -- 1-bit input: High-speed clock
      CLKDIV => dat_clk_i,   -- 1-bit input: Divided Clock
      D      => data,  -- 8-bit input: Parallel Data Input
      RST    => rst_i,        -- 1-bit input: Asynchronous Reset
      T      => '0'         -- 1-bit input: Tristate input from fabric
   );
-----------------------------------------------------------------------------------------
---  <----- UP TO THIS PART
------------------------------------------------------------------------------------------   
end ultrascale_plus;
