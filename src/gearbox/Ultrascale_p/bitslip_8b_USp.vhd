----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/15/2023 04:31:46 PM
-- Design Name: 
-- Module Name: bitslip_8b_USp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
--          0.02 - Corrected bitsliping level
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bitslip_8b_USp is
    Port ( 
       clk_i      : in  std_logic;
       rst_i      : in  std_logic;
       init_i     : in  std_logic;
       data_i     : in  std_logic_vector(7 downto 0);
       init_val_i : in  std_logic_vector(7 downto 0);
       sync_o     : out std_logic;
       data_o     : out std_logic_vector(7 downto 0)
       );
end bitslip_8b_USp;

architecture Behavioral of bitslip_8b_USp is
   signal data_r    : std_logic_vector(15 downto 0);
   signal data_muxd : std_logic_vector(8*8-1 downto 0);
   signal sel_mux_comb : unsigned(3 downto 0);
   signal sel_mux_reg  : unsigned(3 downto 0);
   signal sel_mux_next : unsigned(3 downto 0);
   signal data_aux     : std_logic_vector(7 downto 0);
   signal inv_reg, inv_next: std_logic;
   ----
   type   state_t is (RST_S, INIT_S, INIT_RELEASE_S, REG_MUX_S, VALID_S);
   signal state, next_state : state_t := RST_S;
   ----
   type all_muxed_ch_t is array(natural range<>) of std_logic_vector(7 downto 0);
   signal joined_data  : all_muxed_ch_t(7 downto 0);


   --- DEBUGGING
   attribute mark_debug : string;
   attribute keep : string;
   attribute mark_debug of data_o : signal is "true";

begin

do_longReg: process(clk_i) 
   begin
      if rising_edge(clk_i) then
         if(rst_i = '1') then
            data_r <= (others => '0');
         else
            data_r(15 downto 8) <= data_i;
            data_r( 7 downto 0) <= data_r(15 downto 8);
         end if;
      end if;
   end process;

gen_data_muxed: for i in 0 to 7 generate
   data_muxd(8*(i+1)-1 downto 8*i) <= data_r(7+i downto i); 
   joined_data(i) <= data_muxd(8*(i+1)-1 downto 8*i);
end generate gen_data_muxed;


sel_mux_comb <= x"7" when (data_muxd(63 downto 56) = init_val_i) or (data_muxd(63 downto 56) = not(init_val_i)) else
                x"6" when (data_muxd(55 downto 48) = init_val_i) or (data_muxd(55 downto 48) = not(init_val_i)) else
                x"5" when (data_muxd(47 downto 40) = init_val_i) or (data_muxd(47 downto 40) = not(init_val_i)) else
                x"4" when (data_muxd(39 downto 32) = init_val_i) or (data_muxd(39 downto 32) = not(init_val_i)) else
                x"3" when (data_muxd(31 downto 24) = init_val_i) or (data_muxd(31 downto 24) = not(init_val_i)) else
                x"2" when (data_muxd(23 downto 16) = init_val_i) or (data_muxd(23 downto 16) = not(init_val_i)) else
                x"1" when (data_muxd(15 downto  8) = init_val_i) or (data_muxd(15 downto  8) = not(init_val_i)) else
                x"0" when (data_muxd( 7 downto  0) = init_val_i) or (data_muxd( 7 downto  0) = not(init_val_i)) else
                x"1";

syn_state_proc: process(clk_i)
   begin
      if rising_edge(clk_i) then
         if (rst_i = '1') then
            state       <= RST_S;
            sel_mux_reg <= (others => '0');
            inv_reg     <= '0';
         else
            state       <= next_state;
            sel_mux_reg <= sel_mux_next;
            inv_reg     <= inv_next;
         end if;
      end if;
   end process syn_state_proc;

next_state_decode: process (state, rst_i, init_i, sel_mux_next, sel_mux_comb)
   begin
      next_state <= state;  --default is to stay in current state
      sel_mux_next <= sel_mux_reg;
      case (state) is
         when RST_S =>
            if rst_i = '0' then
               next_state <= INIT_S;
            end if;
         when INIT_S =>
            if  init_i = '1' then
               next_state <= INIT_RELEASE_S;
            end if;
         when INIT_RELEASE_S =>
            if  init_i = '0' then
               next_state <= REG_MUX_S;
               sel_mux_next <= sel_mux_comb;
            end if;   
            
         when REG_MUX_S =>
 --           sel_mux_next <= sel_mux_comb;
            next_state <= VALID_S;
         when VALID_S =>
            next_state <= VALID_S;
            if  init_i = '1' then
               next_state <= INIT_RELEASE_S;
            end if;
         when others =>
            next_state <= RST_S;
      end case;
   end process;


OUTPUT_DECODE: process(clk_i, state, sel_mux_reg, joined_data)
   begin
      case state is
         when RST_S =>
              sync_o <= '0';
              data_aux <= (others => '0');
              inv_next <= '0';
         when INIT_S =>
              sync_o <= '0';
              data_aux <= (others => '0');
              inv_next <= inv_reg;
         when INIT_RELEASE_S =>
              sync_o <= '0';
              data_aux <= (others => '0');
              inv_next <= inv_reg;
         when REG_MUX_S =>
              sync_o <= '0';
              data_aux <= joined_data(to_integer(sel_mux_reg));
              if joined_data(to_integer(sel_mux_reg)) = init_val_i then 
                 inv_next <= '0';
              else
                 inv_next <= '1';
              end if;
         when VALID_S =>
              sync_o <= '1';
              data_aux <= joined_data(to_integer(sel_mux_reg));
              inv_next <= inv_reg;
         when others =>
              sync_o <= '0';
              data_aux <= (others => '0');
              inv_next <= '0';         
      end case;
   end process;

------- Patch for inverted data


with inv_reg select data_o <= 
      joined_data(to_integer(sel_mux_reg))      when '0',
      not(joined_data(to_integer(sel_mux_reg))) when others;
   

--correct_data_level: process (clk_i) 
--begin
--   if rising_edge(clk_i) then
--      if inv_reg = '0' then
--         data_o <= joined_data(to_integer(sel_mux_reg));
--      else
--         data_o <= not(joined_data(to_integer(sel_mux_reg)));
--      end if;
--   end if;
--end process correct_data_level;
end Behavioral;
