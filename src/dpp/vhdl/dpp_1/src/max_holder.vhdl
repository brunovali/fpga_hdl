----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: B. Valinoti -- bvalinot@ictp.it
-- 
-- Create Date: 12/17/2020 08:01:36 PM
-- Design Name: 
-- Module Name: max_holder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity max_holder is
   generic ( D_WIDTH: natural:=12);  --data width 
   port ( 
      clk_i  :  in std_logic;
      rst_i  :  in std_logic;
      clr_i  :  in std_logic;
      data_i :  in std_logic_vector(D_WIDTH-1 downto 0);
      data_o : out std_logic_vector(D_WIDTH-1 downto 0));
end max_holder;

architecture Behavioral of max_holder is
   signal max_r   : std_logic_vector(D_WIDTH-1 downto 0);
begin
   do_max: process (clk_i)
   begin
      if rising_edge(clk_i) then
         if rst_i = '1' then            -- resets maximum value
            max_r <= (others => '0');
         elsif clr_i = '1' then         -- clears maximum value 
            max_r <= (others => '0');
         else
            if data_i > max_r then
               max_r <= data_i;         -- updates maximum value
            else 
               max_r <= max_r;
             end if;
         end if;
      end if;
   end process do_max;
   data_o <= max_r;
end Behavioral;
