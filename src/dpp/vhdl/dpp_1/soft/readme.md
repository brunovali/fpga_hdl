# Pulse Simulator

rev 1.01 - December, 2020
![Creative Commons Attribution 4.0 International Logo](https://i.creativecommons.org/l/by/4.0/80x15.png)

# Table of Contents

* [Usage](#usage)
* [Commands](#commands)
* [Related](#related)

# Usage

Pulse simulator is a CLI python program to visualize and output traces with semi-Gaussian pulses.
Quantity of pulses, the space between the pulses and the additive noise can be customized by the user.

A filename must be provided for the output trace to recorded.

# Commands

All commands can be processed inside the CLI and also as arguments when invoking the program.

## Simulation

Usage: simulation [-h] [-sp num distance noise] [-mp N t0 amp tau offset k dt] [-m model version] [-p] [-v] output.csv

positional arguments:

output.csv            Output file to store the simulated trace.

optional arguments:
-sp num distance noise, --sparams num distance noise
Simulation parameters, number of pulses, distance between pulses, and noise percentage.  
-mp N t0 amp tau offset k dt, --mparams N t0 amp tau offset k dt
Pulse model parameters.  
-m model version, --model model version
Choose the model to simulate {0,1}. 0 for ideal CSA, 1 for exponential CSA.  
-p, --plot            Plot average results and parameters statistics.  
-v, --valid           Create a validation file with t0 and amp for each of the pulses in the trace.

## History

Will display all the commands used in the current session.

## Help

Will display all the commands available.

# Related 

* [PPHAST analyzer](https://gitlab.com/WernerFS/pphast-analyzer)
