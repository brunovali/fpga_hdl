import cmd2, argparse, glob, gc, os, pathlib, sys
import matplotlib.pyplot as plt
import numpy as np

PLOG = 0.2784645427610738

def math_model(x, N: float, t0: float, amp: float, tau: float, offset: float, k: float,
         dt: float):
    """The mathematical model of the semi-Gaussian pulse of ECAL2
    This model includes the pulse plus a reflection and the CSA is modeled as an 
    exponential growth function instead of a step function
    Parameters
    ----------
    x: list
        the time array that will be used to store the calculated values
    N: float
        the number of integrators in the model, in this case is not used as is fixed to 2
    t0: float
        the time of arrival of the pulse
    amp: float
        the amplitude of the pulse
    tau: float
        the exponential time of the pulse, $\tau_{i} = \tau_{d} = \tau_{r} = tau$
    offset: float
        the baseline constant value
    k: float
        the rate of amplitude of the reflected pulse
    dt: float
        the delay time of the reflection
    Returns
    -------
    crrcn: Numpy.array
        a vector containing the simulated values 
    """
    crrcn = np.zeros(np.shape(x))
    norm = (-2 * tau**2 + np.exp((2 *(tau + tau * PLOG))/ tau)*(2 * tau**2 - 4 * tau *(tau + tau * PLOG) \
        + 4*(tau + tau * PLOG)**2))/ np.exp((4 *(tau + tau * PLOG))/ tau)
    for t in x:
        if t < t0:
            crrcn[t] = offset
        elif t0 <= t and t < t0 + dt:
            crrcn[t] = offset + (amp * (np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + 
                np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau))))) / norm
        elif t0 + dt <= t: 
            crrcn[t] = offset + (amp * (np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + 
                np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau)))) + \
                k * amp * (np.exp((-2 * t + (t0 + dt))/tau)*(-2 * np.exp((t0 + dt)/tau)* tau**2 + 
                np.exp(t / tau)*(t**2 + (t0 + dt)**2 + 2 * (t0 + dt) * tau + 2 * tau**2 - 2 * t * ((t0 + dt) + tau)))))/norm
    return crrcn

def crrcn(x, N: float, t0: float, amp: float, tau: float, offset: float, k: float, 
        dt: float):
    """The mathematical model of the semi-Gaussian pulse of ECAL2
    This model contains the pulse plus a reflected pulse and the CSA is modeled a 
    step function

    Parameters
    ----------
    x: list
        the time array that will be used to store the calculated values
    N: float
        the number of integrators in the model
    t0: float
        the time of arrival of the pulse
    amp: float
        the amplitude of the pulse
    tau: float
        the exponential time of the pulse, $\tau_{i} = \tau_{d} = \tau_{r} = tau$
    offset: float
        the baseline constant value
    k: float
        the rate of amplitude of the reflected pulse
    dt: float
        the delay time of the reflection
    Returns
    -------
    crrcn: Numpy.array
        a vector containing the simulated values 
    """
    crrcn = np.zeros(np.shape(x))
    for t in x:
        if t < t0:
            crrcn[t] = offset
        elif t0 <= t and t < t0 + dt:
            crrcn[t] = offset + amp * (np.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * np.exp( - (t - t0) / tau) 
        elif t0 + dt <= t: 
            crrcn[t] = offset + amp * (np.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * np.exp( - (t - t0) / tau) \
                + k * amp * (np.exp(N) / (N ** N)) * ((t - (t0 + dt)) / tau) ** N * np.exp( - (t - (t0+ dt)) / tau)
    return crrcn
class phast_cmd(cmd2.Cmd):
    """Defines the cli class with the extra attributes and functions related to the Simulator app
    Parameters
    ----------
    cmd2.Cmd: Cmd
      Class that defines the basic CLI functionality 

    Returns
    -------
    object
      phast_cmd instance
    """
    def __init__(self):
        history_file = 'persistent_history.cmd2'
        super().__init__(use_ipython= True, persistent_history_file= history_file, 
            allow_cli_args= False)
        self.self_in_py = True
        self.prompt = cmd2.style('P-SIM >: ', fg= cmd2.fg.blue, bold= True)
        self.intro =  cmd2.style(
            'This CLI application is intended to create csv files for simulation assertion.',
            fg= cmd2.fg.green, bg= cmd2.bg.black, bold= True)
        self.allow_style = cmd2.ansi.STYLE_TERMINAL
        cmd2.Cmd.__init__(self)

        # Set the default category name
        self.default_category = 'cmd2 Built-in Commands'

        self.noise = []
        self.model = 0
        self.best_params = []
        
    def do_exit(self, args):
        """Create the exit command to close the app.

        Parameters
        ----------
        None
        Returns
        -------
        None
        """
        self.poutput("Closing")
        self.do_clear_memory(self)
        return True

    sim_parser = argparse.ArgumentParser()
    sim_parser.add_argument('filepath', type = str,
        metavar = 'output.csv', default = 'output.csv',
        help = 'Output file to store the simulated trace.')
    sim_parser.add_argument('-sp', '--sparams', type = float, nargs = 3,
        metavar = ('num','distance','noise'),
        default = (1,0,0),
        help = 'Simulation parameters, number of pulses, distance between pulses, and noise.')
    sim_parser.add_argument('-mp', '--mparams', type = float, nargs = 7 , 
        metavar = ('N','t0' ,'amp', 'tau', 'offset', 'k', 'dt'), help = 'Pulse model parameters.',
        default = (2, 7.1,3000, 2.1, 50, 0, 0))
    sim_parser.add_argument('-m', '--model', type= int, nargs= 1, default= 1, metavar= 'model version',
        help= 'Choose the model to simulate {0,1}. 0 for ideal CSA, 1 for exponential CSA.')
    sim_parser.add_argument('-p', '--plot', action = 'store_true',
        help = 'Plot average results and parameters statistics.')
    sim_parser.add_argument('-v', '--valid', action = 'store_true',
        help = 'Create a validation file with t0 and amp for each of the pulses in the trace.')
    @cmd2.with_argparser(sim_parser)
    def do_simulation(self, args):
        """Create the simulation command to calculate the values according to the model
        Note: validation file pending implementation
        Parameters
        ----------
        args: str
            filepath to save the simulation data, parameters of the simulation, parameters
            for the model function, the option to plot the simulated data and the validation file
            containing the time of arrival and the amplitude of each of the pulses in the simulation
        Returns
        -------
        None
        """
        output = pathlib.Path(args.filepath)
        if args.sparams[0] < 1:
            self.perror("At least one pulse must be simulated!")
            return
        else:
            trace = np.array([])
            if args.model == 0:
                model = crrcn
            elif args.model == 1:
                model = math_model
            for _ in range(int(args.sparams[0])):
                trace = np.append(trace, np.append(model(list(range(32)), *args.mparams) \
                    + np.random.random(32)* args.sparams[2], 
                    np.random.random(int(args.sparams[1]))* args.sparams[2] + args.mparams[4]))
            if args.plot:
                plt.plot(trace, 's')
                plt.show()
            np.savetxt(output, trace, delimiter=',', fmt='%d')
        self.last_result = trace
        return
    
    def complete_simulation(self, text, line, begidx, endidx):
        return self.path_complete(text, line, begidx, endidx, path_filter=os.path.isdir)

def main(argv=None):
    """Entry point for the application

    Parameters
    ----------
    None
    Returns
    -------
    None
    """
    parser = argparse.ArgumentParser()
    command_help = 'optional command to run, if no command given, enter an interactive shell'
    parser.add_argument('command', nargs= '?', help= command_help)
    arg_help = 'optional arguments for command'
    parser.add_argument('command_args', nargs=argparse.REMAINDER, help=arg_help)
    args = parser.parse_args(argv)
    c = phast_cmd()
    sys_exit_code = 0
    if args.command:
        c.onecmd_plus_hooks('{} {}'.format(args.command, ' '.join(args.command_args)))
    else:
        sys_exit_code = c.cmdloop()
    return sys_exit_code

if __name__ == '__main__':
    sys.exit(main())
