-- Clock Domain Crossing data buffer
-- Bruno Valinoti <bvalinot@ictp.it>

library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity adc_cdc is
   generic (
      MODE32     : boolean:=FALSE
   );
   port (
      rst_i      :  in std_logic;
      --
      wr_clk_i   :  in std_logic;
      wr_ena_i   :  in std_logic;
      wr_data1_i :  in std_logic_vector(11 downto 0);
      wr_data2_i :  in std_logic_vector(11 downto 0);
      --
      rd_clk_i   :  in std_logic;
      rd_data1_o : out std_logic_vector(31 downto 0):= (others => '0');
      rd_data2_o : out std_logic_vector(31 downto 0):= (others => '0');
      rd_valid_o : out std_logic;
      rd_error_o : out std_logic
   );
end entity adc_cdc;

architecture XILINX_Serie7 of adc_cdc is

   function getArrayMode(mode32: boolean) return string is
   begin
      if mode32 then
         return "ARRAY_MODE_4_X_8";
      else
         return "ARRAY_MODE_4_X_4";
      end if;
   end function getArrayMode;

   constant ARRAY_MODE : string:=getArrayMode(MODE32);

   signal empty, valid                   : std_logic;
   signal d0, d1, d2, d3, d4, d7, d8     : std_logic_vector(3 downto 0);
   signal q0, q1, q2, q3, q4, q7, q8     : std_logic_vector(7 downto 0);
begin

   -- See UG471 - CH. 3 - IO_FIFO Overview
   in_fifo_inst : IN_FIFO
   generic map (
      ALMOST_EMPTY_VALUE => 1,
      ALMOST_FULL_VALUE  => 1,
      ARRAY_MODE         => ARRAY_MODE,
      SYNCHRONOUS_MODE   => "FALSE"
   )
   port map (
      -- D0-D9: 4-bit (each) input: FIFO inputs
      D0 => d0,
      D1 => d1,
      D2 => d2,
      D3 => d3,
      D4 => d4,
      D5 => "00000000",
      D6 => "00000000",
      D7 => d7,
      D8 => d8,
      D9 => "0000",
      -- Q0-Q9: 8-bit (each) output: FIFO Outputs
      Q0 => q0,
      Q1 => q1,
      Q2 => q2,
      Q3 => q3,
      Q4 => q4,
      Q5 => open,
      Q6 => open,
      Q7 => q7,
      Q8 => q8,
      Q9 => open,
      -- Flags: 1-bit each
      ALMOSTEMPTY => open,
      ALMOSTFULL  => open,
      EMPTY       => empty,
      FULL        => rd_error_o,
      -- FIFO Control Signals: 1-bit (each) input: Clocks, Resets and Enables
      RDCLK => rd_clk_i,
      RDEN  => valid,
      RESET => rst_i,
      WRCLK => wr_clk_i,
      WREN  => wr_ena_i
   );

    
   d0 <= wr_data1_i(11 downto 8);
   d1 <= wr_data1_i(7 downto 4);
   d2 <= wr_data1_i(3 downto 0);

   rd_data1_o(27 downto 24) <= q0(7 downto 4);
   rd_data1_o(23 downto 20) <= q1(7 downto 4);
   rd_data1_o(19 downto 16) <= q2(7 downto 4);
   rd_data1_o(11 downto 08) <= q0(3 downto 0);
   rd_data1_o(07 downto 04) <= q1(3 downto 0);
   rd_data1_o(03 downto 00) <= q2(3 downto 0);

    
   d4 <= wr_data2_i(11 downto 8);
   d7 <= wr_data2_i(7 downto 4);
   d8 <= wr_data2_i(3 downto 0);
   
   rd_data2_o(27 downto 24) <= q4(7 downto 4);
   rd_data2_o(23 downto 20) <= q7(7 downto 4);
   rd_data2_o(19 downto 16) <= q8(7 downto 4);
   rd_data2_o(11 downto 08) <= q4(3 downto 0);
   rd_data2_o(07 downto 04) <= q7(3 downto 0);
   rd_data2_o(03 downto 00) <= q8(3 downto 0);

   valid      <= not empty;
   rd_valid_o <= valid;

end architecture XILINX_Serie7;
