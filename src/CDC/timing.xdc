# ----------------------------------------------------------------------------
# Clock Source - Bank 13
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN Y9 [get_ports sys_clock];  # "GCLK"

## ----------------------------------------------------------------------------
## FMC Expansion Connector - Bank 34
## ----------------------------------------------------------------------------
#set_property PACKAGE_PIN L19 [get_ports {FMC_CLK0_N}];  # "FMC-CLK0_N"
#set_property PACKAGE_PIN L18 [get_ports {FMC_CLK0_P}];  # "FMC-CLK0_P"

#set_property PACKAGE_PIN N20 [get_ports {FMC_LA01_CC_N}];  # "FMC-LA01_CC_N"
#set_property PACKAGE_PIN N19 [get_ports {FMC_LA01_CC_P}];  # "FMC-LA01_CC_P" - corrected 6/6/16 GE

# "FMC-LA00_CC_N" cha_clk_n_i
# "FMC-LA00_CC_P" cha_clk_p_i
set_property PACKAGE_PIN M20 [get_ports cha_clk_n_i_0]  
set_property PACKAGE_PIN M19 [get_ports cha_clk_p_i_0]  
create_clock -period 4.000 -name cha_clk_p_i_0 [get_ports cha_clk_p_i_0]
set_clock_groups -name async -asynchronous -group [get_clocks -include_generated_clocks cha_clk_p_i_0] -group [filter -regexp [all_clocks] {NAME != "cha_clk_p_i_0"}]
set_property IOSTANDARD LVDS_25 [get_ports cha_clk_n_i_0]
set_property IOSTANDARD LVDS_25 [get_ports cha_clk_p_i_0]
set_property DIFF_TERM TRUE     [get_ports cha_clk_n_i_0]
set_property DIFF_TERM TRUE     [get_ports cha_clk_p_i_0]



set_property LOC IN_FIFO_X1Y5  [get_cells design_1_i/test_fmc10x_0/inst/fmc10x_inst/adc_cdc_inst/in_fifo_inst]