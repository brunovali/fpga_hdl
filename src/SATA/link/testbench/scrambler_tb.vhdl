------------------------------------------------------------------------------
----                                                                      ----
----  Scrambler Testbench.                                                ----
----                                                                      ----
----  Author:
----    - Bruno Valinoti, valinoti@inti.gob.ar
----
--------------------------------------------------------------------------
----
---- Copyright (c) 2016 INTI
---- Copyright (c) 2016 Bruno Valinoti
----
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library STD;
use STD.textio.all;
library UTILS;
use UTILS.Simul.all;
library SATA;
use SATA.SATA_Link.all;

entity Scrambler_tb is
end entity Scrambler_tb;

architecture Test of Scrambler_tb is
   constant FREQUENCY   : positive:=1e6;
   signal clk, rst, ena : std_logic;
   signal stop          : boolean;
   signal datao         : std_logic_vector(31 downto 0);
begin

   do_clk_and_rst: Clock
      generic map(FREQUENCY => FREQUENCY)
      port map(clk_o => clk, rst_o => rst, stop_i => stop);

   dut: scrambler
      port map(clk_i => clk, rst_i => rst, ena_i => ena, scramble_o => datao);

   do_test: process
   begin
      ena <= '0';
      wait until rising_edge(clk) and rst='0';
      for i in 0 to 65535 loop
         ena <= '1';
         wait until rising_edge(clk);
         wait for 1 fs;
         print(to_str(datao));
      end loop;
      stop <= TRUE;
      wait;
   end process do_test;

end architecture Test;
