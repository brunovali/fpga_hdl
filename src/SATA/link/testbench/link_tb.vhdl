--------------------------------------------------------------------------
----
----  SATA Link Testbench.
----
----
----  Author:
----    - Bruno Valinoti, valinoti@inti.gob.ar
----
--------------------------------------------------------------------------
----
---- Copyright (c) 2016 INTI
---- Copyright (c) 2016 Bruno Valinoti
----
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library STD;
use STD.textio.all;
library UTILS;
use UTILS.Simul.all;
library SATA;
use SATA.SATA_Link.all;

entity link_tb is
end entity link_tb;

architecture Test of link_tb is
   constant FREQUENCY   : positive:=1e6;
   signal clk, rst, ena : std_logic;
   file   f             : text     ;
   file   fw            : text     ;
   signal stop          : boolean  ;
   signal datao_phy     : std_logic_vector(31 downto 0);
   signal datai_phy     : std_logic_vector(31 downto 0):=x"00000000";
   signal datao_trsp    : std_logic_vector(31 downto 0);
   signal datai_trsp    : std_logic_vector(31 downto 0):=x"00000000";
   signal diag          : std_logic_vector(8 downto 0);
   signal link_good     : std_logic :='0';
   signal link_drdy     : std_logic :='0';
   signal err_8b10b     : std_logic :='0';
   signal phy_err       : std_logic :='0';
   signal cominit       : std_logic :='0';
   signal lnk2phy_prmtv : std_logic :='0';
   signal phy2lnk_prmtv : std_logic :='0';
   signal phy_rdy       : std_logic :='0';
   signal slumber       : std_logic :='0';
   signal partial       : std_logic :='0';
   signal nearfelb      : std_logic :='0';
   signal farafelb      : std_logic :='0';
   signal phy_drdy      : std_logic :='0';
   signal new_trsp_data : std_logic :='0';
   signal datstart     : std_logic;
   signal new_link_data : std_logic :='0';
   signal empty_fifo    : std_logic :='0';
   signal full_fifo     : std_logic :='0';
   signal monitor       : std_logic_vector(31 downto 0):=x"00000000";
   signal rec_pkt       : std_logic_vector(31 downto 0):=x"00000000";


begin
   do_clk_and_rst: Clock
      generic map(FREQUENCY => FREQUENCY)
      port map(clk_o => clk, rst_o => rst, stop_i => stop);

   dut: link_layer
      port map(
      clk_i => clk,
      rst_i => rst,
      ena_i => ena,
      rxd_phy_i    => datai_phy  , -- x"00000000"
      phy_rdy_i    => phy_rdy      ,
      phy_err_i    => phy_err      ,
      cominit_i    => cominit      ,
      lnk_prmtv_i  => phy2lnk_prmtv,
      err_8b10b_i  => err_8b10b    ,
      txd_phy_o    => datao_phy    ,
      slumber_o    => slumber      ,
      partial_o    => partial      ,
      nearfelb_o   => nearfelb     ,
      farafelb_o   => farafelb     ,
      phy_drdy_o   => phy_drdy     ,
      lnk_prmtv_o  => lnk2phy_prmtv,

      diag_o => diag,
      txd_trsp_data_i => datai_trsp   ,
      rxd_trsp_data_o => datao_trsp   ,
      txd_trsp_req_i  => new_trsp_data,
      empty_fifo_i    => empty_fifo,
      full_fifo_i     => full_fifo,
      txd_datstart_o  => datstart    ,
      rxd_trsp_dreq_o => new_link_data
      );

   do_test: process
      variable l        : line;
      variable dataread : std_logic_vector(31 downto 0);
      variable status : file_open_status;       -- Para chequear estado de apertura de arch
      variable ok     : boolean;		-- Para chequeo de lectura de linea
   begin
      file_open(status,f,"../../scripts/linkdata_tx.dat",read_mode);
      assert status=OPEN_OK
         report "No se pudo abrir el archivo de log"
         severity failure;
      ------------
      -- Testbench de transmisión
      -- A_ Test de transmisión de datos estándar sin errores
      ------------
      print("-----------------------");
      print("--- A: DATA Tx TEST ---");
      print("-----------------------");
      ena <= '0';
      wait until rising_edge(clk) and rst='0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      ena <= '1';
      --A1_ Espero hasta que me llgue un ALIGNp para poner PHY RDY = 1
      wait until datao_phy = ALIGNp;
      phy_rdy <= '1';
      wait until rising_edge(clk);
      --A2_ Luego de phy_rdy = '1' debería esperar un numero par de clocks hasta que llegue SYNCp (4)
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait for 1 ns;
      assert datao_phy = SYNCp
         report "Error de inicializacion"
         severity failure;
      --A3_ Dejo pasar algunos ciclos de clock y luego indico nuevo dato disponible desde la capa de transport
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      assert datao_phy = SYNCp
         report "Error durante el estado IDLE"
         severity failure;
      new_trsp_data <= '1';
      readline(f,l);
      read(l,dataread);
      datai_trsp <= dataread;
      -- new_trsp_data <= '1';
      wait until datstart = '1';
     ---A4_ Ahora tengo que chequear el SOF
      assert datao_phy = SOFp
         report "Error: Debería ocurrir el SOF"
         severity failure;
      while not(endfile(f)) loop                -- Leo hasta la última línea con cada clock
         wait until rising_edge(clk);
         readline(f,l);
         read(l,dataread);
         datai_trsp <= dataread;
      end loop;
      new_trsp_data <= '0';
      empty_fifo    <= '1';
      wait until rising_edge(clk);
      --A5_ Una vez que termino de enviar los datos, tengo que chequear que el crc no arroje error y que se envíe EOFp
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait for 1 ns;
      assert datao_phy = EOFp
         report "Error: Debería ocurrir el EOF"
         severity failure;
      wait until rising_edge(clk);
      wait for 1 ns;
      assert datao_phy = WTRMp
         report "Error: Debería ocurrir el WTRMp"
         severity failure;
      file_close(f);
      wait for 10 us;

      ----------------
      ---B_ Test de secuencia de datos estándar con un HOLD entre medio
      ----------------
      print("--------------------");
      print("--- B: HOLD TEST ---");
      print("--------------------");
      file_open(status,f,"../../scripts/linkdata_tx.dat",read_mode);
      assert status=OPEN_OK
         report "No se pudo abrir el archivo de log"
         severity failure;
      ---B1_ Chequeo estado IDLE
      assert datao_phy = SYNCp
         report "Error B1: estado IDLE"
         severity failure;
      ---B2_ Indico nuevo dato disponible desde la capa de transport para tx
      new_trsp_data <= '1';
      empty_fifo    <= '0';
      readline(f,l);
      read(l,dataread);
      datai_trsp <= dataread;

      wait until datstart = '1';
      ---B3_ Ahora tengo que chequear el SOF
      assert datao_phy = SOFp
         report "Error B3: Debería ocurrir el SOF"
         severity failure;
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      empty_fifo    <= '1';
      wait for 3 us;
      assert datao_phy = HOLDp
         report "Error B4: Debería estar enviando HOLDp"
         severity failure;
      empty_fifo    <= '0';
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      new_trsp_data <= '0';
      empty_fifo    <= '1';
      wait until rising_edge(clk); -- DATA
      wait until rising_edge(clk); -- CRC
      wait until rising_edge(clk); -- EOF
      wait for 1 ns;
      assert datao_phy = EOFp
         report "Error B5: Debería ocurrir el EOF"
         severity failure;
      wait until rising_edge(clk); -- WAIT
      wait for 1 ns;
      assert datao_phy = WTRMp
         report "Error B6: Debería ocurrir el WTRMp"
         severity failure;
      file_close(f);
      wait for 10 us;


 ------------
-- Test de error en medio de la transmisión
-- C_ Test de transmisión de datos estándar con errores
      ------------
      print("-----------------------");
      print("--- C: Data Tx ERROR --");
      print("-----------------------");
      wait until rising_edge(clk);
      assert datao_phy = SYNCp
         report "ERROR C1: Error de estao IDLE"
         severity failure;
      empty_fifo    <= '0';
      new_trsp_data <= '1';
      datai_trsp    <= x"00000000";


      wait until datstart = '1';
     ---A4_ Ahora tengo que chequear el SOF
      assert datao_phy = SOFp
         report "Error C2: Debería ocurrir el SOF"
         severity failure;
      -- while not(endfile(f)) loop                -- Leo hasta la última línea con cada clock
      --    wait until rising_edge(clk);
      --    readline(f,l);
      --    read(l,dataread);
      --    datai_trsp <= dataread;
      -- end loop;
      phy_rdy <= '0';
      wait until rising_edge(clk);
      wait for 1 ns;
      assert diag = PHY_NRDY
         report "Error C3: Debería indicarse error por phy no listo"
         severity failure;
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      phy_rdy <= '1';
      wait until datao_phy = ALIGNp;
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until datao_phy = SOFp;
      wait until rising_edge(clk);
      new_trsp_data <= '0';
      empty_fifo    <= '1';
      wait until rising_edge(clk);
      wait until rising_edge(clk);

      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      wait until rising_edge(clk);
      assert datao_phy = WTRMp
         report "Error C4: Debería ocurrir el WTRMp"
         severity failure;
      file_close(f);
      wait for 10 us;
      stop <= TRUE;
      wait;
   end process do_test;


   do_monitor : process(clk)
   begin
      if monitor /= datao_phy then
        monitor <= datao_phy;
        if monitor /= x"00000000" and monitor /= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU" and monitor /= x"B5B5957C" then
           print(to_str(monitor));

        end if;
      end if;
  end process do_monitor;

  do_response : process(clk)
  begin
     if (rising_edge(clk)) then
     if rst = '1' then
        rec_pkt   <= ALIGNp;
        datai_phy <= ALIGNp;
     else
     case(datao_phy) is
          when SYNCp  =>
               rec_pkt <= SYNCp;
          when WTRMp  =>
               rec_pkt <= R_OKp;
          when SOFp   =>
               rec_pkt <= R_IPp;
          when X_RDYp =>
               rec_pkt <= R_RDYp;
          when R_RDYp =>
               rec_pkt <= X_RDYp;
          when R_OKp  =>
               rec_pkt <= R_OKp; --?
          when R_IPp  =>
               rec_pkt <= SYNCp;
          when R_ERRp =>
               rec_pkt <= SYNCp;
          when PMREQ_Sp =>
               rec_pkt <= SYNCp;
          when PMREQ_Pp =>
               rec_pkt <= SYNCp;
          when PMNAKp =>
               rec_pkt <= SYNCp;
          when PMACKp =>
               rec_pkt <= SYNCp;
          when HOLDAp =>
               rec_pkt <= HOLDp;
          when HOLDp =>
               rec_pkt <= HOLDAp;
          when EOFp  =>
               rec_pkt <= R_IPp;
          when DMATp =>
               rec_pkt <= SYNCp;
          when CONTp =>
               rec_pkt <= SYNCp;
          when ALIGNp =>
               rec_pkt <= ALIGNp;
          when others =>
               rec_pkt <= R_IPp;
     end case;
    end if;
     datai_phy <= rec_pkt;
    end if;
  end process do_response;


end architecture Test;
