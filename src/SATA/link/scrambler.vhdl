------------------------------------------------------------------------------
----                                                                      ----
----  SATA Scrambler.                                                     ----
----  Based on Appendix A of SATA Spec 3.0.                               ----
----                                                                      ----
----  Author:                                                             ----
----    - Bruno Valinoti valinoti@inti.gob.ar                             ----
----                                                                      ----
------------------------------------------------------------------------------
----                                                                      ----
---- Copyright (c) 2016 INTI                                              ----
---- Copyright (c) 2016 Bruno Valinoti                                    ----
----                                                                      ----
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Scrambler is
   port(
      clk_i      : in  std_logic; -- Clock
      rst_i      : in  std_logic; -- Reset
      ena_i      : in  std_logic; -- Enable
      scramble_o : out std_logic_vector(31 downto 0) -- Value to use for scramble
       );
end entity Scrambler;

architecture Spec of Scrambler is
begin
   do_scramble:
   process (clk_i)
      variable lfsr    : std_logic_vector(15 downto 0):=x"F0F6";
      variable scramble: std_logic_vector(31 downto 0);
   begin
      if rising_edge(clk_i) then
         if rst_i='1' then
            lfsr:=x"F0F6";
         elsif ena_i='1' then
            scramble(31) := lfsr(12) xor lfsr(10) xor lfsr(7)  xor lfsr(3)  xor lfsr(1)  xor
                            lfsr(0);
            scramble(30) := lfsr(15) xor lfsr(14) xor lfsr(12) xor lfsr(11) xor lfsr(9)  xor
                            lfsr(6)  xor lfsr(3)  xor lfsr(2)  xor lfsr(0);
            scramble(29) := lfsr(15) xor lfsr(13) xor lfsr(12) xor lfsr(11) xor lfsr(10) xor
                            lfsr(8)  xor lfsr(5)  xor lfsr(3)  xor lfsr(2)  xor lfsr(1);
            scramble(28) := lfsr(14) xor lfsr(12) xor lfsr(11) xor lfsr(10) xor lfsr(9)  xor
                            lfsr(7)  xor lfsr(4)  xor lfsr(2)  xor lfsr(1)  xor lfsr(0);
            scramble(27) := lfsr(15) xor lfsr(14) xor lfsr(13) xor lfsr(12) xor lfsr(11) xor
                            lfsr(10) xor lfsr(9)  xor lfsr(8)  xor lfsr(6)  xor lfsr(1)  xor
                            lfsr(0);
            scramble(26) := lfsr(15) xor lfsr(13) xor lfsr(11) xor lfsr(10) xor lfsr(9)  xor
                            lfsr(8)  xor lfsr(7)  xor lfsr(5)  xor lfsr(3)  xor lfsr(0);
            scramble(25) := lfsr(15) xor lfsr(10) xor lfsr(9)  xor lfsr(8)  xor lfsr(7)  xor
                            lfsr(6)  xor lfsr(4)  xor lfsr(3)  xor lfsr(2);
            scramble(24) := lfsr(14) xor lfsr(9)  xor lfsr(8)  xor lfsr(7)  xor lfsr(6)  xor
                            lfsr(5)  xor lfsr(3)  xor lfsr(2)  xor lfsr(1);
            scramble(23) := lfsr(13) xor lfsr(8)  xor lfsr(7)  xor lfsr(6)  xor lfsr(5)  xor
                            lfsr(4)  xor lfsr(2)  xor lfsr(1)  xor lfsr(0);
            scramble(22) := lfsr(15) xor lfsr(14) xor lfsr(7)  xor lfsr(6)  xor lfsr(5)  xor
                            lfsr(4)  xor lfsr(1)  xor lfsr(0);
            scramble(21) := lfsr(15) xor lfsr(13) xor lfsr(12) xor lfsr(6)  xor lfsr(5)  xor
                            lfsr(4)  xor lfsr(0);
            scramble(20) := lfsr(15) xor lfsr(11) xor lfsr(5)  xor lfsr(4);
            scramble(19) := lfsr(14) xor lfsr(10) xor lfsr(4)  xor lfsr(3);
            scramble(18) := lfsr(13) xor lfsr(9)  xor lfsr(3)  xor lfsr(2);
            scramble(17) := lfsr(12) xor lfsr(8)  xor lfsr(2)  xor lfsr(1);
            scramble(16) := lfsr(11) xor lfsr(7)  xor lfsr(1)  xor lfsr(0);
            --
            scramble(15) := lfsr(15) xor lfsr(14) xor lfsr(12) xor lfsr(10) xor lfsr(6)  xor
                            lfsr(3)  xor lfsr(0);
            scramble(14) := lfsr(15) xor lfsr(13) xor lfsr(12) xor lfsr(11) xor lfsr(9)  xor
                            lfsr(5)  xor lfsr(3)  xor lfsr(2);
            scramble(13) := lfsr(14) xor lfsr(12) xor lfsr(11) xor lfsr(10) xor lfsr(8)  xor
                            lfsr(4)  xor lfsr(2)  xor lfsr(1);
            scramble(12) := lfsr(13) xor lfsr(11) xor lfsr(10) xor lfsr(9)  xor lfsr(7)  xor
                            lfsr(3)  xor lfsr(1)  xor lfsr(0);
            scramble(11) := lfsr(15) xor lfsr(14) xor lfsr(10) xor lfsr(9)  xor lfsr(8)  xor
                            lfsr(6)  xor lfsr(3)  xor lfsr(2)  xor lfsr(0);
            scramble(10) := lfsr(15) xor lfsr(13) xor lfsr(12) xor lfsr(9)  xor lfsr(8)  xor
                            lfsr(7)  xor lfsr(5)  xor lfsr(3)  xor lfsr(2)  xor lfsr(1);
            scramble(9)  := lfsr(14) xor lfsr(12) xor lfsr(11) xor lfsr(8)  xor lfsr(7)  xor
                            lfsr(6)  xor lfsr(4)  xor lfsr(2)  xor lfsr(1)  xor lfsr(0);
            scramble(8)  := lfsr(15) xor lfsr(14) xor lfsr(13) xor lfsr(12) xor lfsr(11) xor
                            lfsr(10) xor lfsr(7)  xor lfsr(6)  xor lfsr(5)  xor lfsr(1)  xor
                            lfsr(0);
            scramble(7)  := lfsr(15) xor lfsr(13) xor lfsr(11) xor lfsr(10) xor lfsr(9)  xor
                            lfsr(6)  xor lfsr(5)  xor lfsr(4)  xor lfsr(3)  xor lfsr(0);
            scramble(6)  := lfsr(15) xor lfsr(10) xor lfsr(9)  xor lfsr(8)  xor lfsr(5)  xor
                            lfsr(4)  xor lfsr(2);
            scramble(5)  := lfsr(14) xor lfsr(9)  xor lfsr(8)  xor lfsr(7)  xor lfsr(4)  xor
                            lfsr(3)  xor lfsr(1);
            scramble(4)  := lfsr(13) xor lfsr(8)  xor lfsr(7)  xor lfsr(6)  xor lfsr(3)  xor
                            lfsr(2)  xor lfsr(0);
            scramble(3)  := lfsr(15) xor lfsr(14) xor lfsr(7)  xor lfsr(6)  xor lfsr(5)  xor
                            lfsr(3)  xor lfsr(2)  xor lfsr(1);
            scramble(2)  := lfsr(14) xor lfsr(13) xor lfsr(6)  xor lfsr(5)  xor lfsr(4)  xor
                            lfsr(2)  xor lfsr(1)  xor lfsr(0);
            scramble(1)  := lfsr(15) xor lfsr(14) xor lfsr(13) xor lfsr(5)  xor lfsr(4)  xor
                            lfsr(1)  xor lfsr(0);
            scramble(0)  := lfsr(15) xor lfsr(13) xor lfsr(4)  xor lfsr(0) ;
            --
            lfsr := scramble(31 downto 16);
            scramble_o <= scramble;
         end if;
      end if;
   end process do_scramble;
end architecture Spec;
