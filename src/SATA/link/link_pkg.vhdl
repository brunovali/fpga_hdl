------------------------------------------------------------------------------
----                                                                      ----
----  SATA Link Package.                                                  ----
----                                                                      ----
----  Author:                                                             ----
----    - Rodrigo A. Melo, rmelo@inti.gob.ar                              ----
----    - Bruno Valinoti, valinoti@inti.gob.ar                            ----
------------------------------------------------------------------------------
----                                                                      ----
---- Copyright (c) 2016 INTI                                              ----
---- Copyright (c) 2016 Rodrigo A. Melo                                   ----
----                                                                      ----
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package SATA_Link is
   constant ALIGNp     : std_logic_vector(31 downto 0):=x"7B4A4ABC";
   constant CONTp      : std_logic_vector(31 downto 0):=x"9999AA7C";
   constant DMATp      : std_logic_vector(31 downto 0):=x"3636B57C";
   constant EOFp       : std_logic_vector(31 downto 0):=x"D5D5B57C";
   constant HOLDp      : std_logic_vector(31 downto 0):=x"D5D5AA7C";
   constant HOLDAp     : std_logic_vector(31 downto 0):=x"9595AA7C";
   constant PMACKp     : std_logic_vector(31 downto 0):=x"9595957C";
   constant PMNAKp     : std_logic_vector(31 downto 0):=x"F5F5957C";
   constant PMREQ_Pp   : std_logic_vector(31 downto 0):=x"1717B57C";
   constant PMREQ_Sp   : std_logic_vector(31 downto 0):=x"7575957C";
   constant R_ERRp     : std_logic_vector(31 downto 0):=x"5656B57C";
   constant R_IPp      : std_logic_vector(31 downto 0):=x"5555B57C";
   constant R_OKp      : std_logic_vector(31 downto 0):=x"3535B57C";
   constant R_RDYp     : std_logic_vector(31 downto 0):=x"4A4A957C";
   constant SOFp       : std_logic_vector(31 downto 0):=x"3737B57C";
   constant SYNCp      : std_logic_vector(31 downto 0):=x"B5B5957C";
   constant WTRMp      : std_logic_vector(31 downto 0):=x"5858B57C";
   constant X_RDYp     : std_logic_vector(31 downto 0):=x"5757B57C";

   --- Constantes para determinar errores
   constant  PHY_NRDY      : std_logic_vector(8 downto 0):="000000001";
   constant  ILL_TRNS      : std_logic_vector(8 downto 0):="000000010";
   constant  DMAT_REC      : std_logic_vector(8 downto 0):="000000100";
   constant  ROK_REC       : std_logic_vector(8 downto 0):="000001000";
   constant  RERR_REC      : std_logic_vector(8 downto 0):="000010000";
   constant  SYNC_REC      : std_logic_vector(8 downto 0):="000100000";
   constant  CHK_RDY_ERR   : std_logic_vector(8 downto 0):="001000000";
   constant  WAIT_FIFO_ERR : std_logic_vector(8 downto 0):="010000000";
   constant  ABORTED_REC   : std_logic_vector(8 downto 0):="100000000";


   component CRC is
      port(
         clk_i    : in  std_logic; -- Clock
         rst_i    : in  std_logic; -- Reset
         ena_i    : in  std_logic; -- Enable
         data_i   : in  std_logic_vector(31 downto 0); -- Data Input
         crc_o    : out std_logic_vector(31 downto 0)  -- Accumulated CRC
          );
   end component CRC;

   component Scrambler is
      port(
         clk_i      : in  std_logic; -- Clock
         rst_i      : in  std_logic; -- Reset
         ena_i      : in  std_logic; -- Enable
         scramble_o : out std_logic_vector(31 downto 0) -- Value to use for scramble
          );
   end component Scrambler;

   component link_layer is
      port(
         clk_i      : in  std_logic; -- Clock
         rst_i      : in  std_logic; -- Reset
         ena_i      : in  std_logic; -- Enable

         --PHY side signals
         rxd_phy_i    : in  std_logic_vector(31 downto 0); --Data input from PHY (Rx)
         txd_phy_o    : out std_logic_vector(31 downto 0); --Data output to PHY (Tx)

         slumber_o    : out std_logic;
         partial_o    : out std_logic;
         nearfelb_o   : out std_logic;
         farafelb_o   : out std_logic;
         phy_err_i    : in  std_logic;
         phy_drdy_o   : out std_logic;                     --Link layer data ready for tx
         cominit_i    : in  std_logic;
         lnk_prmtv_o  : out std_logic;
         lnk_prmtv_i  : in  std_logic;
         err_8b10b_i  : in  std_logic;
         phy_rdy_i    : in  std_logic;
         --Transport side signals
         diag_o            : out std_logic_vector(8 downto 0); --Diagnostic error information
         txd_trsp_data_i   : in  std_logic_vector(31 downto 0); --Data input from Transport (Tx)
         rxd_trsp_data_o   : out std_logic_vector(31 downto 0); --Data output to Transport (Rx)
         txd_trsp_req_i    : in  std_logic;                     --Data transmition request
         empty_fifo_i      : in  std_logic;
         full_fifo_i       : in  std_logic;
         txd_datstart_o    : out std_logic;                     --Hold data sequence
         rxd_trsp_dreq_o   : out std_logic                      --New data received
          );
   end component link_layer;

end package SATA_Link;
