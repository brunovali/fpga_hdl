------------------------------------------------------------------------------
----                                                                      ----
----  Paralell implementation of the SATA CRC.                            ----
----  Based on Appendix A of SATA Spec 3.0.                               ----
----                                                                      ----
----  Author:                                                             ----
----    - Bruno Valinoti valinoti@inti.gob.ar                             ----
----                                                                      ----
------------------------------------------------------------------------------
----                                                                      ----
---- Copyright (c) 2016 INTI                                              ----
---- Copyright (c) 2016 Bruno Valinoti                                    ----
----                                                                      ----
------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity CRC is
   port(
      clk_i    : in  std_logic; -- Clock
      rst_i    : in  std_logic; -- Reset
      ena_i    : in  std_logic; -- Enable
      data_i   : in  std_logic_vector(31 downto 0); -- Data Input
      crc_o    : out std_logic_vector(31 downto 0)  -- Accumulated CRC
       );
end entity CRC;

architecture Spec of CRC is
begin
   do_crc:
   process (clk_i)
      variable crc_acc, crc_new : std_logic_vector(31 downto 0):=x"52325032";
   begin
      if rising_edge(clk_i) then
         if rst_i='1' then
            crc_acc:=x"52325032";
         elsif ena_i='1' then
            crc_acc:=crc_acc xor data_i;
            crc_new(31) := crc_acc(31) xor crc_acc(30) xor crc_acc(29) xor crc_acc(28) xor
                           crc_acc(27) xor crc_acc(25) xor crc_acc(24) xor crc_acc(23) xor
                           crc_acc(15) xor crc_acc(11) xor crc_acc(9)  xor crc_acc(8)  xor
                           crc_acc(5);
            crc_new(30) := crc_acc(30) xor crc_acc(29) xor crc_acc(28) xor crc_acc(27) xor
                           crc_acc(26) xor crc_acc(24) xor crc_acc(23) xor crc_acc(22) xor
                           crc_acc(14) xor crc_acc(10) xor crc_acc(8)  xor crc_acc(7)  xor
                           crc_acc(4);
            crc_new(29) := crc_acc(31) xor crc_acc(29) xor crc_acc(28) xor crc_acc(27) xor
                           crc_acc(26) xor crc_acc(25) xor crc_acc(23) xor crc_acc(22) xor
                           crc_acc(21) xor crc_acc(13) xor crc_acc(9)  xor crc_acc(7)  xor
                           crc_acc(6)  xor crc_acc(3);
            crc_new(28) := crc_acc(30) xor crc_acc(28) xor crc_acc(27) xor crc_acc(26) xor
                           crc_acc(25) xor crc_acc(24) xor crc_acc(22) xor crc_acc(21) xor
                           crc_acc(20) xor crc_acc(12) xor crc_acc(8)  xor crc_acc(6)  xor
                           crc_acc(5)  xor crc_acc(2);
            crc_new(27) := crc_acc(29) xor crc_acc(27) xor crc_acc(26) xor crc_acc(25) xor
                           crc_acc(24) xor crc_acc(23) xor crc_acc(21) xor crc_acc(20) xor
                           crc_acc(19) xor crc_acc(11) xor crc_acc(7)  xor crc_acc(5)  xor
                           crc_acc(4)  xor crc_acc(1);
            crc_new(26) := crc_acc(31) xor crc_acc(28) xor crc_acc(26) xor crc_acc(25) xor
                           crc_acc(24) xor crc_acc(23) xor crc_acc(22) xor crc_acc(20) xor
                           crc_acc(19) xor crc_acc(18) xor crc_acc(10) xor crc_acc(6)  xor
                           crc_acc(4)  xor crc_acc(3)  xor crc_acc(0);
            crc_new(25) := crc_acc(31) xor crc_acc(29) xor crc_acc(28) xor crc_acc(22) xor
                           crc_acc(21) xor crc_acc(19) xor crc_acc(18) xor crc_acc(17) xor
                           crc_acc(15) xor crc_acc(11) xor crc_acc(8)  xor crc_acc(3)  xor
                           crc_acc(2);
            crc_new(24) := crc_acc(30) xor crc_acc(28) xor crc_acc(27) xor crc_acc(21) xor
                           crc_acc(20) xor crc_acc(18) xor crc_acc(17) xor crc_acc(16) xor
                           crc_acc(14) xor crc_acc(10) xor crc_acc(7)  xor crc_acc(2)  xor
                           crc_acc(1);
            crc_new(23) := crc_acc(31) xor crc_acc(29) xor crc_acc(27) xor crc_acc(26) xor
                           crc_acc(20) xor crc_acc(19) xor crc_acc(17) xor crc_acc(16) xor
                           crc_acc(15) xor crc_acc(13) xor crc_acc(9)  xor crc_acc(6)  xor
                           crc_acc(1)  xor crc_acc(0);
            crc_new(22) := crc_acc(31) xor crc_acc(29) xor crc_acc(27) xor crc_acc(26) xor
                           crc_acc(24) xor crc_acc(23) xor crc_acc(19) xor crc_acc(18) xor
                           crc_acc(16) xor crc_acc(14) xor crc_acc(12) xor crc_acc(11) xor
                           crc_acc(9)  xor crc_acc(0);
            crc_new(21) := crc_acc(31) xor crc_acc(29) xor crc_acc(27) xor crc_acc(26) xor
                           crc_acc(24) xor crc_acc(22) xor crc_acc(18) xor crc_acc(17) xor
                           crc_acc(13) xor crc_acc(10) xor crc_acc(9)  xor crc_acc(5);
            crc_new(20) := crc_acc(30) xor crc_acc(28) xor crc_acc(26) xor crc_acc(25) xor
                           crc_acc(23) xor crc_acc(21) xor crc_acc(17) xor crc_acc(16) xor
                           crc_acc(12) xor crc_acc(9)  xor crc_acc(8)  xor crc_acc(4);
            crc_new(19) := crc_acc(29) xor crc_acc(27) xor crc_acc(25) xor crc_acc(24) xor
                           crc_acc(22) xor crc_acc(20) xor crc_acc(16) xor crc_acc(15) xor
                           crc_acc(11) xor crc_acc(8)  xor crc_acc(7)  xor crc_acc(3);
            crc_new(18) := crc_acc(31) xor crc_acc(28) xor crc_acc(26) xor crc_acc(24) xor
                           crc_acc(23) xor crc_acc(21) xor crc_acc(19) xor crc_acc(15) xor
                           crc_acc(14) xor crc_acc(10) xor crc_acc(7)  xor crc_acc(6)  xor
                           crc_acc(2);
            crc_new(17) := crc_acc(31) xor crc_acc(30) xor crc_acc(27) xor crc_acc(25) xor
                           crc_acc(23) xor crc_acc(22) xor crc_acc(20) xor crc_acc(18) xor
                           crc_acc(14) xor crc_acc(13) xor crc_acc(9)  xor crc_acc(6)  xor
                           crc_acc(5)  xor crc_acc(1);
            crc_new(16) := crc_acc(30) xor crc_acc(29) xor crc_acc(26) xor crc_acc(24) xor
                           crc_acc(22) xor crc_acc(21) xor crc_acc(19) xor crc_acc(17) xor
                           crc_acc(13) xor crc_acc(12) xor crc_acc(8)  xor crc_acc(5)  xor
                           crc_acc(4)  xor crc_acc(0);
            crc_new(15) := crc_acc(30) xor crc_acc(27) xor crc_acc(24) xor crc_acc(21) xor
                           crc_acc(20) xor crc_acc(18) xor crc_acc(16) xor crc_acc(15) xor
                           crc_acc(12) xor crc_acc(9)  xor crc_acc(8)  xor crc_acc(7)  xor
                           crc_acc(5)  xor crc_acc(4)  xor crc_acc(3);
            crc_new(14) := crc_acc(29) xor crc_acc(26) xor crc_acc(23) xor crc_acc(20) xor
                           crc_acc(19) xor crc_acc(17) xor crc_acc(15) xor crc_acc(14) xor
                           crc_acc(11) xor crc_acc(8)  xor crc_acc(7)  xor crc_acc(6)  xor
                           crc_acc(4)  xor crc_acc(3)  xor crc_acc(2);
            crc_new(13) := crc_acc(31) xor crc_acc(28) xor crc_acc(25) xor crc_acc(22) xor
                           crc_acc(19) xor crc_acc(18) xor crc_acc(16) xor crc_acc(14) xor
                           crc_acc(13) xor crc_acc(10) xor crc_acc(7)  xor crc_acc(6)  xor
                           crc_acc(5)  xor crc_acc(3)  xor crc_acc(2)  xor crc_acc(1);
            crc_new(12) := crc_acc(31) xor crc_acc(30) xor crc_acc(27) xor crc_acc(24) xor
                           crc_acc(21) xor crc_acc(18) xor crc_acc(17) xor crc_acc(15) xor
                           crc_acc(13) xor crc_acc(12) xor crc_acc(9)  xor crc_acc(6)  xor
                           crc_acc(5)  xor crc_acc(4)  xor crc_acc(2)  xor crc_acc(1)  xor
                           crc_acc(0);
            crc_new(11) := crc_acc(31) xor crc_acc(28) xor crc_acc(27) xor crc_acc(26) xor
                           crc_acc(25) xor crc_acc(24) xor crc_acc(20) xor crc_acc(17) xor
                           crc_acc(16) xor crc_acc(15) xor crc_acc(14) xor crc_acc(12) xor
                           crc_acc(9)  xor crc_acc(4)  xor crc_acc(3)  xor crc_acc(1)  xor
                           crc_acc(0);
            crc_new(10) := crc_acc(31) xor crc_acc(29) xor crc_acc(28) xor crc_acc(26) xor
                           crc_acc(19) xor crc_acc(16) xor crc_acc(14) xor crc_acc(13) xor
                           crc_acc(9)  xor crc_acc(5)  xor crc_acc(3)  xor crc_acc(2)  xor
                           crc_acc(0);
            crc_new(9)  := crc_acc(29) xor crc_acc(24) xor crc_acc(23) xor crc_acc(18) xor
                           crc_acc(13) xor crc_acc(12) xor crc_acc(11) xor crc_acc(9)  xor
                           crc_acc(5)  xor crc_acc(4)  xor crc_acc(2)  xor crc_acc(1);
            crc_new(8)  := crc_acc(31) xor crc_acc(28) xor crc_acc(23) xor crc_acc(22) xor
                           crc_acc(17) xor crc_acc(12) xor crc_acc(11) xor crc_acc(10) xor
                           crc_acc(8)  xor crc_acc(4)  xor crc_acc(3)  xor crc_acc(1)  xor
                           crc_acc(0);
            crc_new(7)  := crc_acc(29) xor crc_acc(28) xor crc_acc(25) xor crc_acc(24) xor
                           crc_acc(23) xor crc_acc(22) xor crc_acc(21) xor crc_acc(16) xor
                           crc_acc(15) xor crc_acc(10) xor crc_acc(8)  xor crc_acc(7)  xor
                           crc_acc(5)  xor crc_acc(3)  xor crc_acc(2)  xor crc_acc(0);
            crc_new(6)  := crc_acc(30) xor crc_acc(29) xor crc_acc(25) xor crc_acc(22) xor
                           crc_acc(21) xor crc_acc(20) xor crc_acc(14) xor crc_acc(11) xor
                           crc_acc(8)  xor crc_acc(7)  xor crc_acc(6)  xor crc_acc(5)  xor
                           crc_acc(4)  xor crc_acc(2)  xor crc_acc(1);
            crc_new(5)  := crc_acc(29) xor crc_acc(28) xor crc_acc(24) xor crc_acc(21) xor
                           crc_acc(20) xor crc_acc(19) xor crc_acc(13) xor crc_acc(10) xor
                           crc_acc(7)  xor crc_acc(6)  xor crc_acc(5)  xor crc_acc(4)  xor
                           crc_acc(3)  xor crc_acc(1)  xor crc_acc(0);
            crc_new(4)  := crc_acc(31) xor crc_acc(30) xor crc_acc(29) xor crc_acc(25) xor
                           crc_acc(24) xor crc_acc(20) xor crc_acc(19) xor crc_acc(18) xor
                           crc_acc(15) xor crc_acc(12) xor crc_acc(11) xor crc_acc(8)  xor
                           crc_acc(6)  xor crc_acc(4)  xor crc_acc(3)  xor crc_acc(2)  xor
                           crc_acc(0);
            crc_new(3)  := crc_acc(31) xor crc_acc(27) xor crc_acc(25) xor crc_acc(19) xor
                           crc_acc(18) xor crc_acc(17) xor crc_acc(15) xor crc_acc(14) xor
                           crc_acc(10) xor crc_acc(9)  xor crc_acc(8)  xor crc_acc(7)  xor
                           crc_acc(3)  xor crc_acc(2)  xor crc_acc(1);
            crc_new(2)  := crc_acc(31) xor crc_acc(30) xor crc_acc(26) xor crc_acc(24) xor
                           crc_acc(18) xor crc_acc(17) xor crc_acc(16) xor crc_acc(14) xor
                           crc_acc(13) xor crc_acc(9)  xor crc_acc(8)  xor crc_acc(7)  xor
                           crc_acc(6)  xor crc_acc(2)  xor crc_acc(1)  xor crc_acc(0);
            crc_new(1)  := crc_acc(28) xor crc_acc(27) xor crc_acc(24) xor crc_acc(17) xor
                           crc_acc(16) xor crc_acc(13) xor crc_acc(12) xor crc_acc(11) xor
                           crc_acc(9)  xor crc_acc(7)  xor crc_acc(6)  xor crc_acc(1)  xor
                           crc_acc(0);
            crc_new(0)  := crc_acc(31) xor crc_acc(30) xor crc_acc(29) xor crc_acc(28) xor
                           crc_acc(26) xor crc_acc(25) xor crc_acc(24) xor crc_acc(16) xor
                           crc_acc(12) xor crc_acc(10) xor crc_acc(9)  xor crc_acc(6)  xor
                           crc_acc(0);
            crc_acc:=crc_new;
            crc_o <= crc_acc;
         end if;
      end if;
   end process do_crc;
end architecture Spec;
