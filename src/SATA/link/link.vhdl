--------------------------------------------------------------------------
----
----  SATA Link.
----
----
----  Author:
----    - Bruno Valinoti, valinoti@inti.gob.ar
----
--------------------------------------------------------------------------
----
---- Copyright (c) 2016 INTI
---- Copyright (c) 2016 Bruno Valinoti
----
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library SATA;
use SATA.SATA_Link.all;

entity link_layer is
   port(
      clk_i        : in  std_logic; -- Clock
      rst_i        : in  std_logic; -- Reset
      ena_i        : in  std_logic; -- Enable

      --PHY side signals
      rxd_phy_i    : in  std_logic_vector(31 downto 0); --Data input from PHY (Rx)
      txd_phy_o    : out std_logic_vector(31 downto 0); --Data output to PHY (Tx)

      slumber_o    : out std_logic;
      partial_o    : out std_logic;
      nearfelb_o   : out std_logic;
      farafelb_o   : out std_logic;
      phy_err_i    : in  std_logic;
      phy_drdy_o   : out std_logic;                     --Link layer data ready for tx
      cominit_i    : in  std_logic;
      lnk_prmtv_o  : out std_logic;                     -- primitive signaling, always '1' unless data is being transmitted
      lnk_prmtv_i  : in  std_logic;
      err_8b10b_i  : in  std_logic;
      phy_rdy_i    : in  std_logic;
      --Transport side signals
      diag_o            : out std_logic_vector(8 downto 0); --Diagnostic error information
      txd_trsp_data_i   : in  std_logic_vector(31 downto 0); --Data input from Transport (Tx)
      rxd_trsp_data_o   : out std_logic_vector(31 downto 0); --Data output to Transport (Rx)
      txd_trsp_req_i    : in  std_logic;                     --Data transmition request
      empty_fifo_i      : in  std_logic;
      full_fifo_i       : in  std_logic;
      txd_datstart_o   : out std_logic;                     --Hold data sequence 1:new data/0:no data (level)
      rxd_trsp_dreq_o   : out std_logic                      --New data received  1:new data/0:no data (level)
       );
end entity link_layer;

architecture Spec of link_layer is

--Tipo usado para la maquina de estados de idle
   type st_link is (L_IDLE_S, L_SyncEscape_S, L_NoCommErr_S, L_NoComm_S, L_SendAlign_S, L_RESET_S,
                    HL_SendChkRdy_S, DL_SendChkRdy_s, L_SendSOF_S, L_SendData_S, L_RcvrHold_S,
                    L_SendHold_S, L_SendCRC_S, L_SendEOF_S, L_Wait_S,
                    L_RcvChkRdy_S, L_RcvWaitFifo_S, L_RcvData_S, L_Hold_S, L_RcvHold_S, L_RcvEOF_S,
                    L_GoodCRC_S, L_GoodEnd_S, L_BadEnd_S,
                    L_TPMPartial_S, L_TPMSlumber_S, L_PMOff_S, L_PMDeny_S, L_ChkPhyRdy_S,
                    L_NoCommPower_S, L_WakeUp1_S, L_WakeUp2_S, L_NoPmnak_S
                    );
   signal link_st : st_link;

--Tipo usado para la maquina de estados de recepcion de datos
   type st_dword_decode is (IDLE_Rx, SOF_Rx, DATA_REC_Rx, CONT_Rx, HOLD_REC_Rx, CRC_CHECK_Rx, WAIT_TRM_Rx);
   signal link_rx_st : st_dword_decode;

--Tipo usado para la maquina de estados de transmision de datos
   type st_dword_encode is (IDLE_Tx, XRDY_Tx, SOF_Tx, DATA_PAY_Tx, HOLD_Tx, CRC_Tx, EOF_Tx, WTRM_Tx);
   signal link_tx_st : st_dword_encode;

--- Señales usadas para la instanciación de los diferentes componentes
   signal    scrmb_rst_rx       : std_logic:='0';  -- Scrambler rx reset sequence
   signal    scrmb_ena_rx       : std_logic:='0';  -- Scrambler rx enable signal
   signal    scrmb_rst_tx       : std_logic:='0';  -- Scrambler tx reset sequence
   signal    scrmb_ena_tx       : std_logic:='0';  -- Scrambler tx enable signal
   signal    crc_ena_rx         : std_logic:='0';  -- CRC rx enable signal
   signal    crc_ena_tx         : std_logic:='0';  -- CRC tx enable signal
   signal    crc_rst_rx         : std_logic:='0';  -- CRC rx reset
   signal    crc_rst_tx         : std_logic:='0';  -- CRC tx reset
   signal    int_crc_rst_rx     : std_logic:='0';
   signal    int_scrmb_rst_rx   : std_logic:='0';
   signal    int_crc_rst_tx     : std_logic:='0';
   signal    int_scrmb_rst_tx   : std_logic:='0';
   signal    cominit_scrmb_rst  : std_logic:='0';
   signal    cont_scrmb_ena     : std_logic:='0';
   signal    scrmb_rst_cont     : std_logic:='0';

   signal    dword_reg       : std_logic_vector(31 downto 0);
   signal    dword_reg_tx    : std_logic_vector(31 downto 0);
   signal    scramble_rx     : std_logic_vector(31 downto 0);
   signal    scramble_tx     : std_logic_vector(31 downto 0);
   signal    scramble_cont   : std_logic_vector(31 downto 0);
   signal    crc_feed_rx     : std_logic_vector(31 downto 0);
   signal    crc_feed_tx     : std_logic_vector(31 downto 0);
   signal    crc_check       : std_logic_vector(31 downto 0);
   signal    crc_gen_tx      : std_logic_vector(31 downto 0);
   signal    primitive       : std_logic_vector(31 downto 0);
   signal    primitive_last  : std_logic_vector(31 downto 0);
   signal    primitive_cont  : unsigned(2 downto 0);           --Primitive repetition counter (for CONTp trigger)


   signal    trsp_tx_req     : std_logic:='0';  -- Transmission request from Transport layer
   signal    trsp_av_dword   : std_logic:='0';  -- Avaiable Dword for transmission from Transport layer
   signal    trsp_comm_err   : std_logic:='0';  -- non ready condition of phy layer while attempting to process another state
   signal    trsp_mpty_fifo  : std_logic:='0';  -- Transport empty FIFO signaling
   signal    trsp_full_fifo  : std_logic:='0';  -- Transport full FIFO signaling
   signal    trsp_esc_frame  : std_logic:='0';  -- Current frame transfer escape signaling
   signal    trsp_good       : std_logic:='0';  -- Transport layer good result
   signal    trsp_ok_FIS     : std_logic:='0';  -- Transport layer FIS recognizement flag
   signal    trsp_ok_status  : std_logic:='0';  -- Transport Status indication OK
   signal    trsp_wkup_req   : std_logic:='0';  -- Transport wakeup request
   signal    phy_rdy         : std_logic:='0';  -- PHY ready
   signal    phy_rx_av       : std_logic:='0';  -- New data avaiable from phy (received)
   signal    phy_x_rdy       : std_logic:='0';  -- Transmission data ready (X_RDYp) ¡payload ready for transmission!
   signal    phy_r_rdy       : std_logic:='0';  -- Receiver ready (R_RDY) ¡Current node is ready to receive!
   signal    phy_syn_rdy     : std_logic:='0';  -- SYNC_p received
   signal    phy_pwr_rdy     : std_logic:='0';  -- PHY power mangement modify ready
   signal    phy_pwr_ena     : std_logic:='0';  -- PHY power mangement modify enable
   signal    phy_rst         : std_logic:='0';  -- PHY reset
   signal    phy_ok          : std_logic:='0';  -- No error detected in received payload (R_OK) from PHY
   signal    phy_err         : std_logic:='0';  -- Reception error in received payload (R_ERR) from PHY
   signal    phy_sof         : std_logic:='0';  -- Reception of SOF primitive from PHY
   signal    phy_eof         : std_logic:='0';  -- Reception of EOF primitive from PHY
   signal    phy_cont        : std_logic:='0';  -- Reception of CONT primitive from PHY
   signal    phy_rip         : std_logic:='0';  -- Reception in progress, data is being received
   signal    phy_wtrm        : std_logic:='0';  -- Wait for frame termination
   signal    phy_pmnak       : std_logic:='0';  -- Power management denial
   signal    phy_pmak        : std_logic:='0';  -- Power management acknowledge
   signal    phy_hold        : std_logic:='0';  -- Reception of HOLD primitive from PHY
   signal    phy_holda       : std_logic:='0';  -- Reception of HOLDA primitive from PHY
   signal    phy_pmreq_p     : std_logic:='0';  -- Power management request to partial (PMREQ_Pp)
   signal    phy_pmreq_s     : std_logic:='0';  -- Power management req to slumber (PMREQ_S)
   signal    phy_dmat        : std_logic:='0';  -- Request to the transmitter to terminate a DMA data transmission (received from phy)
   signal    phy_comwake     : std_logic:='0';  -- COMWAKE signal reception flag

   signal    pmack_count     : unsigned ( 3 downto 0 ) ;
   signal    sof_tx_done     : std_logic:='0';  -- SOF transmitted
   signal    rst_active      : std_logic:='0';  -- Reset control active
   signal    any_dword       : std_logic:='0';  -- Any DWORD received
   signal    dat_dword       : std_logic:='0';  -- Dat DWORD received
   signal    crc_tx_done     : std_logic:='0';  -- CRC transmitted flag
   signal    crc_done        : std_logic:='0';  -- CRC done
   signal    crc_error       : std_logic:='0';  -- CRC no valid

   signal    eof_tx_done     : std_logic:='0';  -- EOF transmitted flag

   signal    phy_trsp_err    : std_logic:='0';  -- Any PHY or Transport error occurrence

   signal    comm_init_done  : std_logic:='0';
   signal    err_decode      : std_logic:='0';  --Decoding error
   signal    diag            : std_logic_vector (8 downto 0); -- Diagnostic comm state vector

begin

   dut_CRC_rx : crc
      port map (
                clk_i => clk_i        ,
                rst_i => crc_rst_rx   ,
                data_i => crc_feed_rx ,
                ena_i => crc_ena_rx   ,
                crc_o => crc_check
                 );
   dut_SCRAMBLER_rx: scrambler
      port map (
                clk_i => clk_i        ,
                rst_i => scrmb_rst_rx ,
                ena_i => scrmb_ena_rx ,
                scramble_o => scramble_rx
               );

   dut_CRC_tx : crc
      port map (
                clk_i => clk_i        ,
                rst_i => crc_rst_tx   ,
                data_i => crc_feed_tx ,
                ena_i => crc_ena_tx   ,
                crc_o => crc_gen_tx
                 );
   dut_SCRAMBLER_tx: scrambler
      port map (
                clk_i => clk_i        ,
                rst_i => scrmb_rst_tx ,
                ena_i => scrmb_ena_tx ,
                scramble_o => scramble_tx
               );

  -- These two scramblers are for CONT usage
   -- dut_SCRAMBLERcONT_tx: scrambler
   --    port map (
   --              clk_i => clk_i        ,
   --              rst_i =>  ,
   --              ena_i =>  ,
   --              scramble_o =>
   --              );
   dut_SCRAMBLERcONT_tx: scrambler
      port map (
                clk_i => clk_i,
                rst_i => scrmb_rst_cont,
                ena_i => cont_scrmb_ena,
                scramble_o => scramble_cont
                );
-------------------------------------------------------------------
--- crc/scramble input selection
------------------------------------------------------------------
with scrmb_ena_rx select
     crc_feed_rx <= (scramble_rx xor dword_reg) when '1',
                    (x"00000000" xor rxd_phy_i) when others;

crc_feed_tx <= dword_reg_tx;
-------------------------------------------------------------------
--- Reset signals
------------------------------------------------------------------
crc_rst_rx     <= rst_i or int_crc_rst_rx;
scrmb_rst_rx   <= rst_i or int_scrmb_rst_rx or cominit_scrmb_rst;
crc_rst_tx     <= rst_i or int_crc_rst_tx;
scrmb_rst_tx   <= rst_i or int_scrmb_rst_tx or cominit_scrmb_rst;
scrmb_rst_cont <= rst_i or cominit_scrmb_rst;

-----
--COMINIT syncronyzer
-----
scrm_rst:
   process(clk_i)
   begin
      if(rising_edge(clk_i)) then
         if (cominit_i = '1') then
            cominit_scrmb_rst <= '1';
         else
            cominit_scrmb_rst <= '0';
         end if;
      end if;
end process scrm_rst;
-------------------------------------------------------------------
--- Internal signals assignation
------------------------------------------------------------------
phy_rdy <= phy_rdy_i;

-------------------------------------------------------------------
--- New received data detector
------------------------------------------------------------------
phy_rx_av   <= '0' when rxd_phy_i = SYNCp else '1';
phy_eof     <= '0';

-------------------------------------------------------------------
--- New data for transmission
------------------------------------------------------------------
trsp_tx_req     <=  txd_trsp_req_i;
trsp_av_dword   <= not(empty_fifo_i);
trsp_mpty_fifo  <= empty_fifo_i;
trsp_full_fifo  <= full_fifo_i ;

-------------------------------------------------------------------
--- Diagnostic register & status signals
------------------------------------------------------------------
phy_drdy_o  <= '0';
diag_o      <= diag;

-------------------------------------------------------------------
--- Primitive signaling
------------------------------------------------------------------
lnk_prmtv_o <= '0' when link_st = L_SendData_S else
               '0' when link_st = L_SendCRC_S  else
               '1';

-------------------------------------------------------------------
--- Receive data fsm
------------------------------------------------------------------
rx_data_fsm:
   process(clk_i)
   begin
       if(rising_edge(clk_i) or cominit_i = '1') then
          if(rst_i = '1') then
             link_rx_st <= IDLE_Rx;
             dword_reg <= rxd_phy_i; -- Registro el dato de entrada
          else
             any_dword        <= '1';  -- TODO: esta puesto en 1 para primeras etapas de simulacion
             dat_dword        <= '1';  -- TODO: esta puesto en 1 para primeras etapas de simulacion
             rxd_trsp_data_o  <= x"00000000";
             rxd_trsp_dreq_o <= '0';
             int_scrmb_rst_rx <= '0';
             scrmb_ena_rx     <= '0';
             crc_ena_rx       <= '0';
             phy_cont         <= '0';
             phy_sof          <= '0';
             phy_dmat         <= '0';
             phy_x_rdy        <= '0';
             phy_err          <= '0';
             phy_rip          <= '0';
             phy_ok           <= '0';
             phy_r_rdy        <= '0';
             phy_pmreq_p      <= '0';
             phy_pmreq_s      <= '0';
             trsp_esc_frame   <= '0';
             phy_holda        <= '0';
             phy_holda        <= '0';
             phy_syn_rdy      <= '0';
             case(link_rx_st) is
                 when IDLE_Rx      =>
                      link_rx_st  <= IDLE_Rx;
                      case (rxd_phy_i) is
                           when SOFp   =>   -- Si recibo el SOF entonces paso al estado de recepcion del data payload
                                 phy_sof      <= '1';
                              -- crc_ena_rx   <= '1';
                           when X_RDYp =>
                                link_rx_st <= SOF_Rx;
                                phy_x_rdy <= '1';
                           when R_ERRp =>
                                phy_err <= '1';
                           when R_IPp  =>
                                phy_rip <= '1';
                           when R_RDYp =>
                                phy_r_rdy  <= '1';
                           when R_OKp =>
                                phy_ok <= '1';
                           when SYNCp =>
                                phy_syn_rdy <= '1';
                           when PMREQ_Sp =>
                                phy_pmreq_s <= '1';
                           when PMREQ_Pp =>
                                phy_pmreq_p <= '1';
                           when DMATp  =>
                                phy_dmat <= '1';
                                trsp_esc_frame <= '1';
                           when HOLDp  =>
                                phy_hold <= '1';
                           when HOLDAp =>
                                phy_holda <= '1';
                           when others  =>
                      end case;
                 when SOF_Rx  =>
                      phy_sof      <= '1';
                      scrmb_ena_rx <= '1';
                      crc_ena_rx   <= '1';
                      link_rx_st <= DATA_REC_Rx;
                      dword_reg <= rxd_phy_i;
                 when DATA_REC_Rx  =>
                      crc_ena_rx    <= '0';
                      scrmb_ena_rx  <= '0';
                      if(rxd_phy_i = EOFp) then  -- crc_tx_done
                         link_rx_st <= CRC_CHECK_Rx;
                         dword_reg <= dword_reg;
                      elsif(rxd_phy_i = HOLDp) then
                          link_rx_st <= HOLD_REC_Rx;
                          phy_hold <= '1';  --HOLD recibido
                      elsif (rxd_phy_i = CONTp) then
                          link_rx_st <= CONT_Rx;
                          phy_cont <= '1';
                      else
                          dword_reg <= rxd_phy_i;
                          rxd_trsp_data_o  <= rxd_phy_i;
                          rxd_trsp_dreq_o <= '1';  -- Indico nuevo dato recibido a la capa transport para que lo agregue a la FIFO
                          crc_ena_rx      <= '1';
                          scrmb_ena_rx    <= '1';
                      end if;
                 when CONT_Rx      =>
                      phy_cont <= '1';
                      primitive_last <= primitive_last;
                      if (rxd_phy_i = CONTp) then
                         link_rx_st <= CONT_Rx;
                      else
                         -- Aca tengo que analizar por todas las otras primitivas?
                         link_rx_st <= IDLE_Rx;
                      end if;
                 when HOLD_REC_Rx  =>
                      if rxd_phy_i = HOLDp then
                         link_rx_st <= HOLD_REC_Rx;
                         phy_hold <= '1';
                      else
                         dword_reg <= rxd_phy_i;
                         rxd_trsp_data_o  <= rxd_phy_i;
                         crc_ena_rx    <= '1';
                         scrmb_ena_rx  <= '1';
                      end if;
                 when CRC_CHECK_Rx =>
                      -- link_rx_st <= WAIT_SOF_Rx;
                      link_rx_st <= IDLE_Rx;
                      crc_done  <= '1';
                      int_scrmb_rst_rx <= '1';
                      if (crc_check = x"00000000") then
                         crc_error <= '0';
                      else
                         crc_error <= '1';
                      end if;
                 when others      =>
                      link_rx_st <= IDLE_Rx;

            end case;
          end if;
       end if;
   end process rx_data_fsm;

------------------------------------------------------------------
--- Transmit data fsm
------------------------------------------------------------------
tx_data_fsm:
   process(clk_i)
      variable txd_phy : std_logic_vector(31 downto 0);
   begin
      if(rising_edge(clk_i)) then
         if (rst_i = '1' or cominit_i = '1') then
            link_tx_st <= IDLE_Tx;
            int_scrmb_rst_tx <= '1';
            int_crc_rst_tx   <= '1';
            txd_phy          := ALIGNp;
            primitive_cont   <= "000";
         else
            crc_ena_tx       <= '0';
            txd_datstart_o   <= '0';
            int_crc_rst_tx   <= '0';
            int_scrmb_rst_tx <= '0';
            sof_tx_done      <= '0';
            crc_tx_done      <= '0';
            eof_tx_done      <= '0';
            case link_tx_st is
                when IDLE_Tx      =>
                      link_tx_st <= IDLE_Tx;
                      txd_phy    := primitive;
                      if (trsp_tx_req and trsp_av_dword and comm_init_done) = '1' then
                         link_tx_st <= XRDY_Tx;
                         primitive_cont <= "000";
                      elsif(phy_pmreq_s = '1' or phy_pmreq_p = '1') then
                         txd_phy   := PMNAKp;
                      elsif phy_hold = '1' then
                         txd_phy   := HOLDAp;
                      end if;
                 when XRDY_Tx      =>
                      link_tx_st   <= XRDY_Tx;
                      txd_phy      := primitive;
                      scrmb_ena_tx <= '1';
                      if primitive_cont < "010" then
                         primitive_cont <= primitive_cont + "001";
                      else
                         primitive_cont <= primitive_cont;
                      end if;
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                      elsif phy_r_rdy = '1' then
                         link_tx_st <= SOF_Tx;
                         scrmb_ena_tx   <= '1';
                         primitive_cont <= "000";
                      end if;
                 when SOF_Tx       =>
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                      else
                         link_tx_st    <= DATA_PAY_Tx;
                         txd_phy       := SOFp;
                         dword_reg_tx   <= txd_trsp_data_i;
                         crc_ena_tx     <= '1';
                         scrmb_ena_tx   <= '1';
                         sof_tx_done    <= '1';
                         txd_datstart_o <= '1';
                      end if;
                 when DATA_PAY_Tx  =>
                      if trsp_tx_req = '0' then
                         link_tx_st   <= CRC_Tx;
                         txd_phy := (dword_reg_tx xor scramble_tx);
                         crc_ena_tx <= '0';
                         primitive_cont <= "000";
                      else
                         if comm_init_done = '0' then
                            link_tx_st <= IDLE_Tx;
                         else
                            if trsp_av_dword = '1' then
                               link_tx_st   <= DATA_PAY_Tx;
                               txd_phy := (dword_reg_tx xor scramble_tx);
                               crc_ena_tx   <= '1';
                               scrmb_ena_tx <= '1';
                               dword_reg_tx <= txd_trsp_data_i;
                            else
                               link_tx_st <= HOLD_Tx;
                               txd_phy    := HOLDp;  -- Transmito el HOLD
                               crc_ena_tx <= '0';
                               primitive_cont <= "000";
                            end if;
                         end if;
                      end if;
                 when HOLD_Tx      =>
                      txd_phy := HOLDp;
                      if primitive_cont < "010" then
                         primitive_cont <= primitive_cont + "001";
                      else
                         primitive_cont <= primitive_cont;
                      end if;
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                         primitive_cont <= "000";
                      elsif trsp_av_dword = '1' then
                         link_tx_st <= DATA_PAY_Tx;
                         dword_reg_tx <= txd_trsp_data_i;
                         primitive_cont <= "000";
                      else
                         link_tx_st <= HOLD_Tx;
                      end if;
                 when CRC_Tx       =>
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                      else
                         link_tx_st   <= EOF_Tx;
                         txd_phy      := (crc_gen_tx xor scramble_tx);
                         scrmb_ena_tx <= '0';
                         crc_tx_done  <= '1';
                      end if;
                 when EOF_Tx       =>
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                      else
                         link_tx_st  <= WTRM_Tx ;
                         txd_phy     := EOFp    ;
                         int_scrmb_rst_tx <= '1';
                         eof_tx_done      <= '1';
                         scrmb_ena_tx     <= '0';
                     end if;
                 when WTRM_Tx      =>
                      if comm_init_done = '0' then
                         link_tx_st <= IDLE_Tx;
                      else
                         link_tx_st <= WTRM_Tx ;
                         txd_phy    := WTRMp   ;
                         int_crc_rst_tx   <= '1';
                         if primitive_cont < "010" then
                            primitive_cont <= primitive_cont + "001";
                         else
                            primitive_cont <= primitive_cont;
                         end if;
                         if phy_ok = '1' then
                            link_tx_st <= IDLE_Tx ;
                            txd_phy    := SYNCp   ;
                            primitive_cont <= "000"   ;
                         end if;
                      end if;
                 when others        =>
                      link_tx_st <= IDLE_Tx  ;
                      txd_phy    := primitive;
            end case;
----------------------------------------------------------
-- PARA TESTEO SE DEJA COMENTADO EL MULTIPLEXOR DEL CONTp
-- queda estático el txd_phy a la salida  -->
----------------------------------------------------------
            -- if (primitive_cont > "001") and (link_tx_st /= IDLE_Tx)  then
            --    txd_phy_o <= (CONTp xor scramble_cont);
            --    cont_scrmb_ena <= '1';
            -- elsif link_st = L_SendAlign_S then   -- Para inicializacion del scramble_cont
            --    cont_scrmb_ena <= '1';
            -- else
            --    txd_phy_o <= txd_phy;
            --    cont_scrmb_ena <= '0';
            -- end if; <--

            txd_phy_o <= txd_phy;
          end if;
       end if;
   end process tx_data_fsm;

-- -------------------------------------------------------------------
-- --- Link fsm
-- ------------------------------------------------------------------
link_fsm:
   process(clk_i)
   begin
      if(rising_edge(clk_i) or cominit_i = '1') then
         if rst_i = '1' then
            link_st <= L_IDLE_S;
            primitive <= ALIGNp;
         else
            diag <= "000000000";
            case(link_st) is
                when L_IDLE_S      =>
                     primitive <= SYNCp;
                     link_st <= L_IDLE_S; --L1:7
                     if (phy_rdy = '1') then
                        if (trsp_tx_req = '1') then  --L1:1a
                           link_st <= HL_SendChkRdy_S;
                           primitive <= X_RDYp;
                        elsif (phy_pmreq_p = '1') then  --L1:2  --TODO: A este pedido tendría que rechazarlo
                           -- link_st <= L_TPMPartial_S;
                           link_st <= L_IDLE_S;
                        elsif (phy_pmreq_s = '1') then  --L1:3  --TODO: A este pedido tendría que rechazarlo
                           -- link_st <= L_TPMSlumber_S;
                           link_st <= L_IDLE_S;
                        end if;
                     elsif (phy_rdy = '0') then --L1:8
                        link_st <= L_NoCommErr_S;
                     elsif (phy_x_rdy = '1') then  --L1:4
                        link_st <= L_RcvWaitFifo_S;
                     end if;
                when L_SyncEscape_S   =>
                     if(phy_rx_av = '1' and ((phy_syn_rdy = '0') or (phy_x_rdy = '0'))) then  --L2:1
                        link_st <= L_SyncEscape_S;
                     elsif ((phy_syn_rdy = '1') or (phy_x_rdy = '1')) then  --L2:2
                        link_st <= L_IDLE_S;
                     elsif (phy_rdy = '0') then  --L2:3
                        link_st <= L_NoCommErr_S;
                        diag <= PHY_NRDY;
                     end if;
                when L_NoCommErr_S =>  --LS1
                     comm_init_done <= '0';
                     diag <= PHY_NRDY;
                     link_st <= L_NoComm_S;
                when L_NoComm_S    =>
                     primitive <= ALIGNp;
                     if(phy_rdy = '0') then  --LS2:1
                        link_st <= L_NoComm_S;
                     else  --LS2:2
                        link_st <= L_SendAlign_S;
                     end if;
                when L_SendAlign_S =>
                     primitive <= ALIGNp;
                     if(phy_rdy = '0') then  --LS3:1
                        link_st <= L_NoCommErr_S;
                     else  --LS3:2
                        link_st <= L_IDLE_S;
                        comm_init_done <= '1';
                     end if;
                when L_RESET_S     =>
                     if(rst_active = '1') then
                        link_st <= L_RESET_S;
                     else
                        link_st <= L_NoComm_S;
                     end if;
-- -------------------------------------------------------------------
-- --- Transmision
-- ------------------------------------------------------------------
                 when HL_SendChkRdy_S =>
                      primitive <= X_RDYp;
                      if(phy_r_rdy = '1' ) then  --LT1:1
                         link_st   <= L_SendSOF_S;
                      elsif(phy_x_rdy = '1' ) then  --LT1:2
                         link_st <= L_RcvWaitFifo_S;
                      elsif(any_dword = '1' ) then  --LT1:3
                         link_st <= HL_SendChkRdy_S;
                      elsif(phy_rdy = '1') then    --LT1:4
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      end if;
                 when DL_SendChkRdy_s =>  -- Link layer transmits X_RDY and waits for R_RDY from the Phy layer
                      primitive <= X_RDYp;
                      if(phy_rdy = '0')then   --LT2:3
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(phy_r_rdy = '1') then  --LT2:1
                         link_st <= L_SendSOF_S;
                      elsif( any_dword ='1') then  --LT2:2
                         link_st <= DL_SendChkRdy_s;
                      end if;
                 when L_SendSOF_S     =>
                      primitive <= SOFp;
                      if(phy_rdy = '0') then  --LT3:2
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(sof_tx_done = '1' ) then  --LT3:1
                         link_st <= L_SendData_S;
                      elsif(phy_syn_rdy = '1' ) then  --LT3:3
                          link_st <= L_IDLE_S;
                          diag <= ILL_TRNS;
                      end if;
                 when L_SendData_S    =>
                      primitive <= X_RDYp;
                      if(any_dword ='1' and phy_syn_rdy = '0' and trsp_av_dword = '1' and phy_hold = '0' and phy_dmat = '0') then --LT4:1
                         link_st <= L_SendData_S;
                      elsif(phy_hold = '1') then   --LT4:2
                         link_st <= L_RcvrHold_S;
                      elsif( any_dword = '1' and phy_syn_rdy ='0' and trsp_av_dword = '0' and trsp_tx_req = '1') then   --LT4:3
                         link_st <= L_SendHold_S;
                      elsif(any_dword = '1' and phy_syn_rdy ='0' and trsp_mpty_fifo = '1') then  --LT4:4
                         if(phy_dmat = '1') then
                            link_st <= L_SendHold_S;
                            diag <= DMAT_REC;
                         else
                            link_st <= L_SendCRC_S;
                         end if;
                      elsif(phy_syn_rdy = '1') then  --LT4:5
                         link_st <= L_IDLE_S;
                         diag <= ILL_TRNS;
                      elsif(phy_rdy = '0' ) then  --LT4:6
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(trsp_esc_frame = '1' ) then  --LT4:7
                         link_st <= L_SyncEscape_S;
                      end if;
                 when L_RcvrHold_S    =>
                      primitive <= HOLDAp;
                      if(   phy_hold = '0' and err_decode = '0' and phy_syn_rdy = '0' and any_dword ='1' and phy_dmat = '0' and trsp_av_dword = '1' ) then  --LT5:1
                         link_st <= L_SendData_S;
                      elsif(phy_hold = '1' or  err_decode = '1') then  --LT5:2
                         link_st <= L_RcvrHold_S;
                      elsif(phy_syn_rdy = '1') then  --LT5:3
                         link_st <= L_IDLE_S;
                         diag <= ILL_TRNS;
                      elsif(phy_dmat = '1') then  --LT5:4
                         link_st <= L_SendCRC_S;
                         diag <= DMAT_REC;
                      elsif( phy_rdy = '0') then  --LT5:5
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(trsp_esc_frame = '1') then  --LT5:6
                         link_st <= L_SyncEscape_S;
                      -- elsif(phy_syn_rdy = '1') then  --LT5:7  ¿IGUAL AL LT5:3?
                      --    link_st <= L_IDLE_S;
                      --    diag <= ILL_TRNS;
                      end if;
                 when L_SendHold_S    =>
                      if(any_dword = '1' and phy_hold = '0' and phy_syn_rdy = '0' and trsp_av_dword = '1' ) then --LT6:1
                         link_st <= L_SendData_S;
                      elsif(phy_hold = '1' and trsp_av_dword = '1') then  --LT6:2
                         link_st <= L_RcvrHold_S;
                      elsif(trsp_av_dword = '0' and any_dword = '1' and phy_syn_rdy = '0') then  --LT6:3
                         link_st <= L_SendHold_S;
                      elsif(trsp_tx_req = '0' and any_dword = '1' and phy_syn_rdy = '0') then  --LT6:4
                          if(phy_dmat = '1') then
                           diag <= DMAT_REC;
                          end if;
                         link_st <= L_SendCRC_S;
                      elsif(phy_syn_rdy = '1') then  --LT6:5
                         link_st <= L_IDLE_S;
                         diag <= ILL_TRNS;
                      elsif(phy_rdy = '0') then  --LT6:6
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(trsp_esc_frame = '1') then  --LT6:7
                         link_st <= L_SyncEscape_S;
                      end if;
                 when L_SendCRC_S     =>
                      --Aca tengo que transmitir el CRC
                      if(crc_tx_done = '1') then     --LT7:1
                         link_st <= L_SendEOF_S;
                      elsif(phy_rdy = '0') then  --LT7:2
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      elsif(phy_syn_rdy = '1') then  --LT7:3
                         link_st <= L_IDLE_S;
                         diag <= ILL_TRNS;
                      end if;
                 when L_SendEOF_S     =>
                      primitive <= EOFp;
                      if(eof_tx_done = '1') then     --LT8:1
                         link_st <= L_Wait_S;
                      elsif(phy_rdy = '0') then  --LT8:2
                         link_st <= L_NoCommErr_S ;
                         diag <= PHY_NRDY;
                      elsif(phy_syn_rdy = '1') then  --LT8:3
                         link_st <= L_IDLE_S;
                         diag <= ILL_TRNS;
                      end if;
                 when L_Wait_S        =>
                      primitive <= WTRMp;
                      if(phy_ok = '1') then     --LT9:1
                         link_st  <= L_IDLE_S ;
                         diag   <= ROK_REC  ;
                         primitive <= SYNCp   ;
                      elsif(phy_err = '1') then  --LT9:2
                         link_st <= L_IDLE_S;
                         diag <= RERR_REC;
                      elsif(phy_syn_rdy = '1') then  --LT9:3
                         link_st <= L_IDLE_S;
                         diag  <= SYNC_REC;
                      elsif(any_dword = '1' and phy_ok = '0' and phy_err = '0' and phy_syn_rdy = '0') then  --LT9:4
                         link_st <= L_Wait_S;
                      elsif(phy_rdy = '0') then  --LT9:5
                         link_st <= L_NoCommErr_S;
                         diag <= PHY_NRDY;
                      end if;
-- -------------------------------------------------------------------
-- --- Reception states
-- ------------------------------------------------------------------
              when L_RcvChkRdy_S   =>
                   primitive <= R_RDYp;
                   if(phy_rdy = '0') then           --LR1:4
                      link_st <= L_NoCommErr_S;
                      diag <= PHY_NRDY;
                   elsif(phy_x_rdy = '1') then          --LR1:1
                      link_st <= L_RcvChkRdy_S;
                   elsif(phy_sof = '1' ) then          --LR1:2
                      link_st <= L_RcvData_S;
                   elsif(any_dword = '1' and phy_sof = '0' and phy_x_rdy = '0') then   --LR1:3
                      link_st <= L_IDLE_S;
                      diag <= CHK_RDY_ERR;
                   end if;
              when L_RcvWaitFifo_S =>
                   primitive <= SYNCp;
                   if(phy_rdy = '0' ) then        --LR2:4
                      link_st <= L_NoCommErr_S;
                      diag <= PHY_NRDY;
                   elsif(phy_x_rdy = '1') then
                      if(trsp_full_fifo = '0') then      --LR2:1
                         link_st <= L_RcvChkRdy_S;
                      else   --LR2:2
                         link_st <= L_RcvWaitFifo_S;
                      end if;
                   elsif(any_dword = '1') then      --LR2:3
                      link_st <= L_IDLE_S;
                      diag <= WAIT_FIFO_ERR;
                   else
                      link_st <= L_RcvWaitFifo_S;
                   end if;
              when L_RcvData_S     =>
                   primitive <= R_IPp;
                   if(phy_rdy = '0' ) then         --LR3:8
                     link_st <=  L_NoCommErr_S;
                     diag <= PHY_NRDY;
                   elsif((trsp_full_fifo = '0' and dat_dword = '1') or phy_dmat = '1') then     --LR3:1
                      link_st <= L_RcvData_S;
                   elsif(phy_hold = '1' ) then        --LR3:3
                     link_st <= L_RcvHold_S;
                   elsif(trsp_full_fifo = '1' and dat_dword = '1') then  --LR3:2
                      link_st <= L_Hold_S;
                   elsif(phy_eof = '1' ) then         --LR3:4
                      link_st <= L_RcvEOF_S;
                   elsif(phy_wtrm = '1' ) then        --LR3:5
                      link_st <= L_BadEnd_S ;
                   elsif(phy_syn_rdy = '1' ) then     --LR3:6
                      link_st <=  L_IDLE_S;
                      diag <= ABORTED_REC;
                    elsif(phy_holda = '0' and any_dword = '1' ) then   --LR3:7
                      link_st <= L_RcvData_S;
                   elsif(trsp_esc_frame = '1' ) then  --LR3:9
                      link_st <= L_SyncEscape_S;
                      primitive <= DMATp;
                   end if;
              when L_Hold_S        =>
                   primitive <= HOLDp;
                   if(phy_rdy = '0' ) then          --LR4:5
                      link_st <=  L_NoCommErr_S;
                      diag <= PHY_NRDY;
                   elsif(phy_hold = '0' and any_dword = '1' and trsp_full_fifo = '0' ) then   --LR4:1
                      link_st <= L_RcvData_S;
                   elsif(phy_hold = '1' and trsp_full_fifo = '0' ) then --LR4:2
                      link_st <= L_RcvHold_S;
                   elsif(phy_eof = '1' ) then          --LR4:3
                      link_st <= L_RcvEOF_S;
                   elsif(trsp_full_fifo = '1' ) then   --LR4:4
                      link_st <= L_Hold_S;
                   elsif(phy_syn_rdy = '1' ) then      --LR4:6
                      link_st <= L_IDLE_S;
                      diag <= ILL_TRNS;
                   elsif( trsp_esc_frame = '1') then   --LR4:7
                      link_st <= L_SyncEscape_S;
                      primitive <= DMATp;
                   else
                      link_st <= L_Hold_S;
                   end if;
              when L_RcvHold_S     =>
                   primitive <= HOLDp;
                   if(phy_rdy = '0' ) then          --LR5:5
                      link_st <= L_NoCommErr_S;
                      diag <= PHY_NRDY;
                   elsif(phy_hold = '1' ) then         --LR5:2
                      link_st <= L_RcvHold_S;
                   elsif(phy_syn_rdy = '1' ) then      --LR5:4
                     link_st <= L_IDLE_S;
                     diag  <= ILL_TRNS;
                   elsif(phy_eof = '1'  ) then         --LR5:3
                      link_st <= L_RcvEOF_S;
                   elsif(trsp_esc_frame = '1' ) then   --LR5:6
                      link_st   <= L_SyncEscape_S;
                      primitive <= DMATp;
                   elsif(any_dword = '1') then    --LR5:1
                      link_st <= L_RcvData_S;
                   end if;
              when L_RcvEOF_S      =>
                  primitive <= R_IPp;
                  if(phy_rdy = '0' ) then   --LR6:4
                     link_st <= L_NoCommErr_S;
                     diag <= PHY_NRDY;
                  elsif(crc_done = '0' ) then   --LR6:1
                     link_st <= L_RcvEOF_S;
                  elsif(crc_error = '0') then   --LR6:2
                     link_st <= L_GoodCRC_S;
                  else   --LR6:3
                     link_st <= L_BadEnd_S;
                  end if;
              when L_GoodCRC_S     =>
                  primitive <= R_IPp;
                  if(phy_rdy = '0' ) then   --LR7:4
                     link_st <= L_NoCommErr_S;
                     diag <= PHY_NRDY;
                  elsif(trsp_ok_status = '0' ) then   --LR7:3
                    link_st <= L_GoodCRC_S;
                  else
                     if(trsp_good = '1' ) then   --LR7:1
                        link_st <= L_GoodEnd_S;
                     elsif(trsp_ok_FIS = '0' ) then   --LR7:2
                        link_st <= L_BadEnd_S;
                     elsif(phy_trsp_err = '1' ) then   --LR7:5
                        link_st <= L_BadEnd_S;
                     elsif(phy_syn_rdy = '1' ) then   --LR7:6
                        link_st <= L_IDLE_S;
                        diag <= ILL_TRNS;
                     else
                        link_st <= L_GoodCRC_S;
                     end if;
                  end if;
              when L_GoodEnd_S     =>
                  primitive <= R_OKp;
                  if(phy_rdy = '0' ) then   --LR8:3
                     link_st <= L_NoCommErr_S;
                     diag <= PHY_NRDY;
                  elsif(phy_syn_rdy = '1' ) then   --LR8:1
                     link_st <= L_IDLE_S;
                  elsif(any_dword = '1') then   --LR8:2
                     link_st <= L_GoodEnd_S;
                  end if;
              when L_BadEnd_S      =>
                  if(phy_rdy = '0' ) then   --LR9:3
                     link_st <= L_NoCommErr_S;
                     diag <= PHY_NRDY;
                  elsif(phy_syn_rdy = '1' ) then   --LR9:1
                     link_st <= L_IDLE_S;
                  elsif(any_dword = '1') then   --LR9:2
                     link_st <= L_BadEnd_S;
                  end if;
-- ------------------------------------------------------------------
              when others      =>
                  link_st <= L_IDLE_S;
         end case;
      end if;
   end if;
  end process link_fsm;

end architecture Spec;
